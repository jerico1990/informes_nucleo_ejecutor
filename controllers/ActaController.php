<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Settings;
use yii\helpers\Json;


class ActaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public $web_url_servicio = "http://fspreset.minagri.gob.pe:5000";
    //public $web_url_servicio = "http://si2.minagri.gob.pe:4000";

    public function actionActaVerificacion($id_intervencion=null)
    {
        date_default_timezone_set('America/Lima');
        setlocale (LC_TIME,"spanish");
        $phpWord = new PhpWord();
        $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/formatos/ACTA_VERIFICACION_TECNICA.docx');
        
        $json_cabecera = [];
        $json_detalle_actividades = [];

        $txt_cut = "";
        $txt_nombre_nucleo = "";
        $cod_amcrd = "";
        $txt_localidad = "";
        $txt_dist = "";
        $txt_prov = "";
        $txt_dpto = "";
        $imp_meta_km = "";


        /* Seteando servicio de cabecera del nucleo ejecutor */

        $curl_cabecera = curl_init();
        curl_setopt($curl_cabecera, CURLOPT_URL, $this->web_url_servicio . "/nue_intervencion");
        curl_setopt($curl_cabecera, CURLOPT_POST, TRUE);
        curl_setopt($curl_cabecera, CURLOPT_POSTFIELDS, "id_intervencion=".$id_intervencion."");
        curl_setopt($curl_cabecera, CURLOPT_RETURNTRANSFER, true);
        $remote_server_cabecera = curl_exec ($curl_cabecera);
        $json_cabecera_validar = Json::decode($json = $remote_server_cabecera);
        if(count($json_cabecera_validar)==0){
            return json_encode(['msg'=>'No encontrado','flg'=>0]);
        }
        
        if($remote_server_cabecera){
            $json_cabecera = Json::decode($json = $remote_server_cabecera);
            $txt_nombre_nucleo = $json_cabecera[0]["TXT_NOMBRE_NUCLEO"];
            $txt_cut = "";
            $cod_amcrd = $json_cabecera[0]["COD_AMCRD"];
            $txt_localidad = $json_cabecera[0]["TXT_LOCALIDAD_POLITICA"];
            $txt_dist = $json_cabecera[0]["TXT_DIST"];
            $txt_prov = $json_cabecera[0]["TXT_PROV"];
            $txt_dpto = $json_cabecera[0]["TXT_DPTO"];
            $imp_meta_km = $json_cabecera[0]["IMP_META_KM"];
        }
        curl_close ($curl_cabecera);
        $template->setValue('TXT_NOMBRE_NUCLEO', $txt_nombre_nucleo);
        $template->setValue('COD_AMCRD', $cod_amcrd);
        $template->setValue('TXT_LOCALIDAD_POLITICA', $txt_localidad);
        $template->setValue('TXT_DIST', $txt_dist);
        $template->setValue('TXT_PROV', $txt_prov);
        $template->setValue('TXT_DPTO', $txt_dpto);
        $template->setValue('IMP_META_KM', $imp_meta_km);


        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $template->saveAs($temp_file);
        header("Content-Disposition: attachment; filename=ACTA_VERIFICACION.docx");
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file);  // remove temp file
        return true;
    }
}
