<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Settings;
use yii\helpers\Json;


class AnexoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    
	public $web_url_servicio = "http://fspreset.minagri.gob.pe:5000";
	//public $web_url_servicio = "http://si2.minagri.gob.pe:4000";
	
    public function actionAnexoFicha04($id_intervencion=null)
    {
        
        date_default_timezone_set('America/Lima');
        setlocale (LC_TIME,"spanish");

        $phpWord = new PhpWord();
        $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/formatos/FICHA_ANEXO_04.docx');
		
		/*Inicializando las variables */
		$txt_actividad = "";
		$txt_ne = "";
		$txt_er = "";
		$txt_dpto = "";
		$txt_prov = "";
		$txt_dist = "";
		$txt_localidad_politica = "";
		$txt_junta_usuario_politica = "";
		$txt_comision_regante_politica = "";
		$txt_comite_riego_politica = "";
		$dbl_nro_revestido = "";
		$dbl_nro_tierra = "";
		$dbl_cantidad_total = "";
		$txt_anio_construccion = "";
		$txt_caudal = "";
		$txt_ancho = "";
		$txt_altura = "";
		$int_id_tipo_seccion_1 = "";
		$int_id_tipo_seccion_2 = "";
		$txt_zona_utm = "";
		$txt_coordenada_inicio_norte = "";
		$txt_coordenada_fin_norte = "";
		$txt_coordenada_inicio_este = "";
		$txt_coordenada_fin_este = "";
		$int_nro_usuarios = "";
		$int_nro_familias = "";
		$dbl_nro_hectareas = "";
		$dbl_nro_jornales = "";
		$int_nro_dias_calendario = "";
		$id_ficha = "";
		$dbl_costo_indirecto = "";


		$curl_cabecera = curl_init();
        curl_setopt($curl_cabecera, CURLOPT_URL,$this->web_url_servicio . "/nue_ficha");
        curl_setopt($curl_cabecera, CURLOPT_POST, TRUE);
        curl_setopt($curl_cabecera, CURLOPT_POSTFIELDS, "id_intervencion=".$id_intervencion."");
        curl_setopt($curl_cabecera, CURLOPT_RETURNTRANSFER, true);
        $remote_server_cabecera = curl_exec ($curl_cabecera);
        $json_cabecera_validar = Json::decode($json = $remote_server_cabecera);
        if(count($json_cabecera_validar)==0){
            return json_encode(['msg'=>'No encontrado','flg'=>0]);
            //var_dump($json_cabecera_validar);die;
		}

		if($remote_server_cabecera){
            $json_cabecera = Json::decode($json = $remote_server_cabecera);
            $id_ficha = $json_cabecera[0]["ID_FICHA"];
            $txt_actividad = $json_cabecera[0]["TXT_ACTIVIDAD"];
			$txt_ne = $json_cabecera[0]["TXT_NE"];
			$txt_er = $json_cabecera[0]["TXT_ER"];
			$txt_dpto = $json_cabecera[0]["TXT_DPTO"];
			$txt_prov = $json_cabecera[0]["TXT_PROV"];
			$txt_dist = $json_cabecera[0]["TXT_DIST"];
			$txt_localidad_politica = $json_cabecera[0]["TXT_LOCALIDAD_POLITICA"];
			$txt_junta_usuario_politica = $json_cabecera[0]["TXT_JUNTA_USUARIO_POLITICA"];
			$txt_comision_regante_politica = $json_cabecera[0]["TXT_COMISION_REGANTE_POLITICA"];
			$txt_comite_riego_politica = $json_cabecera[0]["TXT_COMITE_RIEGO_POLITICA"];
			$dbl_nro_revestido = $json_cabecera[0]["DBL_NRO_REVESTIDO"];
			$dbl_nro_tierra = $json_cabecera[0]["DBL_NRO_TIERRA"];
			$dbl_cantidad_total = $json_cabecera[0]["DBL_CANTIDAD_TOTAL"];
			$txt_anio_construccion = $json_cabecera[0]["TXT_ANIO_CONSTRUCCION"];
			$txt_caudal = $json_cabecera[0]["TXT_CAUDAL"];
			$txt_ancho = $json_cabecera[0]["TXT_ANCHO"];
			$txt_altura = $json_cabecera[0]["TXT_ALTURA"];
			$dbl_costo_indirecto = $json_cabecera[0]["DBL_COSTO_INDIRECTO"];

			switch ($json_cabecera[0]["INT_ID_TIPO_SECCION"]) {
				case 1:
					$int_id_tipo_seccion_1 = 'x';
					break;
				case 2:
					$int_id_tipo_seccion_2 = 'x';
					break;
			}
			
			$txt_zona_utm = $json_cabecera[0]["TXT_ZONA_UTM"];
			$txt_coordenada_inicio_norte = $json_cabecera[0]["TXT_COORDENADA_INICIO_NORTE"];
			$txt_coordenada_fin_norte = $json_cabecera[0]["TXT_COORDENADA_FIN_NORTE"];
			$txt_coordenada_inicio_este = $json_cabecera[0]["TXT_COORDENADA_INICIO_ESTE"];
			$txt_coordenada_fin_este = $json_cabecera[0]["TXT_COORDENADA_FIN_ESTE"];
			$int_nro_usuarios = $json_cabecera[0]["INT_NRO_USUARIOS"];
			$int_nro_familias = $json_cabecera[0]["INT_NRO_FAMILIAS"];
			$dbl_nro_hectareas = $json_cabecera[0]["DBL_NRO_HECTAREAS"];
			$dbl_nro_jornales = $json_cabecera[0]["DBL_NRO_JORNALES"];
			$int_nro_dias_calendario = $json_cabecera[0]["INT_NRO_DIAS_CALENDARIO"];
        }
        
		curl_close ($curl_cabecera);
		
		$template->setValue('TXT_ACTIVIDAD', $txt_actividad);
		$template->setValue('TXT_NE', $txt_ne);
		$template->setValue('TXT_ER', $txt_er);
		$template->setValue('TXT_DPTO', $txt_dpto);
		$template->setValue('TXT_PROV', $txt_prov);
		$template->setValue('TXT_DIST', $txt_dist);
		$template->setValue('TXT_LOCALIDAD_POLITICA', $txt_localidad_politica);
		$template->setValue('TXT_JUNTA_USUARIO_POLITICA', $txt_junta_usuario_politica);
		$template->setValue('TXT_COMISION_REGANTE_POLITICA', $txt_comision_regante_politica);
		$template->setValue('TXT_COMITE_RIEGO_POLITICA', $txt_comite_riego_politica);
		$template->setValue('DBL_NRO_REVESTIDO', $dbl_nro_revestido);
		$template->setValue('DBL_NRO_TIERRA', $dbl_nro_tierra);
		$template->setValue('DBL_CANTIDAD_TOTAL', $dbl_cantidad_total);
		$template->setValue('TXT_ANIO_CONSTRUCCION', $txt_anio_construccion);
		$template->setValue('TXT_CAUDAL', $txt_caudal);
		$template->setValue('TXT_ANCHO', $txt_ancho);
		$template->setValue('TXT_ALTURA', $txt_altura);
		$template->setValue('INT_ID_TIPO_SECCION_1', $int_id_tipo_seccion_1);
		$template->setValue('INT_ID_TIPO_SECCION_2', $int_id_tipo_seccion_2);
		$template->setValue('TXT_ZONA_UTM', $txt_zona_utm);
		$template->setValue('TXT_COORDENADA_INICIO_NORTE', $txt_coordenada_inicio_norte);
		$template->setValue('TXT_COORDENADA_FIN_NORTE', $txt_coordenada_fin_norte);
		$template->setValue('TXT_COORDENADA_INICIO_ESTE', $txt_coordenada_inicio_este);
		$template->setValue('TXT_COORDENADA_FIN_ESTE', $txt_coordenada_fin_este);
		$template->setValue('INT_NRO_USUARIOS', $int_nro_usuarios);
		$template->setValue('INT_NRO_FAMILIAS', $int_nro_familias);
		$template->setValue('DBL_NRO_HECTAREAS', $dbl_nro_hectareas);
		$template->setValue('DBL_NRO_JORNALES', $dbl_nro_jornales);
		$template->setValue('INT_NRO_DIAS_CALENDARIO', $int_nro_dias_calendario);

		/* Seteando lista de presupuesto */

        $curl_detalle_presupuestos = curl_init();
        curl_setopt($curl_detalle_presupuestos, CURLOPT_URL,$this->web_url_servicio . "/nue_lst_presupuesto");
        curl_setopt($curl_detalle_presupuestos, CURLOPT_POST, TRUE);
        curl_setopt($curl_detalle_presupuestos, CURLOPT_POSTFIELDS, "id_ficha=".$id_ficha."");
        curl_setopt($curl_detalle_presupuestos, CURLOPT_RETURNTRANSFER, true);
        $remote_server_detalle_presupuestos = curl_exec ($curl_detalle_presupuestos);

        if($remote_server_detalle_presupuestos){
            $json_detalle_presupuestos = Json::decode($json = $remote_server_detalle_presupuestos);
		}
		
		$dbl_subtotal_presupuesto = 0.00;
		if(count($json_detalle_presupuestos)>0){
            $template->cloneRow('DBL_NRO_REGISTRO', count($json_detalle_presupuestos));
            $rows = 1 ;
            foreach($json_detalle_presupuestos as $presupuesto){
				$template->setValue('DBL_NRO_REGISTRO#'.$rows.'', "".ucwords($presupuesto['DBL_NRO_REGISTRO'])."");
				$template->setValue('TXT_DESCRIPCION#'.$rows.'', "".ucwords($presupuesto['TXT_DESCRIPCION'])."");
				$template->setValue('TXT_UNIDAD_MEDIDA#'.$rows.'', "".ucwords($presupuesto['TXT_UNIDAD_MEDIDA'])."");
				$template->setValue('DBL_CANTIDAD#'.$rows.'', "".ucwords($presupuesto['DBL_CANTIDAD'])."");
				$template->setValue('DBL_COSTO_UNITARIO#'.$rows.'', "".ucwords(number_format($presupuesto['DBL_COSTO_UNITARIO'], 2, '.', ','))."");
				$template->setValue('DBL_COSTO_PARCIAL#'.$rows.'', "".ucwords(number_format($presupuesto['DBL_COSTO_PARCIAL'], 2, '.', ','))."");
				$dbl_subtotal_presupuesto = $dbl_subtotal_presupuesto + $presupuesto['DBL_COSTO_PARCIAL'];
                $rows++;
			}
			$template->setValue('DBL_SUBTOTAL_PRESUPUESTO', "".ucwords(number_format($dbl_subtotal_presupuesto, 2, '.', ','))."");
		}
		

		/* Seteando lista de distribucion */

        $curl_detalle_distribuciones = curl_init();
        curl_setopt($curl_detalle_distribuciones, CURLOPT_URL,$this->web_url_servicio . "/nue_lst_distribucion_gasto");
        curl_setopt($curl_detalle_distribuciones, CURLOPT_POST, TRUE);
        curl_setopt($curl_detalle_distribuciones, CURLOPT_POSTFIELDS, "id_ficha=".$id_ficha."");
        curl_setopt($curl_detalle_distribuciones, CURLOPT_RETURNTRANSFER, true);
        $remote_server_detalle_distribuciones = curl_exec ($curl_detalle_distribuciones);

        if($remote_server_detalle_distribuciones){
            $json_detalle_distribuciones = Json::decode($json = $remote_server_detalle_distribuciones);
		}
		
		$dbl_subtotal_distribucion = 0.00;
		$dbl_cd = 0.00;
		$dbl_ci = 0.00;
		$dbl_total = 0.00;

		if(count($json_detalle_distribuciones)>0){
            $template->cloneRow('DBL_NRO_REGISTRO_D', count($json_detalle_distribuciones));
            $rows = 1 ;
            foreach($json_detalle_distribuciones as $distribucion){
				$template->setValue('DBL_NRO_REGISTRO_D#'.$rows.'', "".ucwords($distribucion['DBL_NRO_REGISTRO'])."");
				$template->setValue('TXT_DESCRIPCION_D#'.$rows.'', "".ucwords($distribucion['TXT_DESCRIPCION'])."");
				$template->setValue('TXT_UNIDAD_MEDIDA_D#'.$rows.'', "".ucwords($distribucion['TXT_UNIDAD_MEDIDA'])."");
				$template->setValue('DBL_CANTIDAD_D#'.$rows.'', "".ucwords($distribucion['DBL_CANTIDAD'])."");
				$template->setValue('DBL_COSTO_UNITARIO_D#'.$rows.'', "".ucwords(number_format($distribucion['DBL_COSTO_UNITARIO'], 2, '.', ','))."");
				$template->setValue('DBL_COSTO_PARCIAL_D#'.$rows.'', "".ucwords(number_format($distribucion['DBL_COSTO_PARCIAL'], 2, '.', ','))."");
				$dbl_subtotal_distribucion = $dbl_subtotal_distribucion + $distribucion['DBL_COSTO_PARCIAL'];
                $rows++;
			}
			$template->setValue('DBL_SUBTOTAL_DISTRIBUCION_D', "".ucwords(number_format($dbl_subtotal_distribucion, 2, '.', ','))."");
		}
		
		$dbl_cd = $dbl_subtotal_presupuesto;
		//$dbl_ci = ($dbl_subtotal_presupuesto*0.1);
		$dbl_total = $dbl_cd + $dbl_costo_indirecto;

		$template->setValue('DBL_CD', "".ucwords(number_format($dbl_cd, 2, '.', ','))."");
		$template->setValue('DBL_CI', "".ucwords(number_format($dbl_costo_indirecto, 2, '.', ','))."");
		$template->setValue('DBL_TOTAL', "".ucwords(number_format($dbl_total, 2, '.', ','))."");

        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $template->saveAs($temp_file);
        header("Content-Disposition: attachment; filename=FICHA_ANEXO_04.docx");
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file);  // remove temp file
        return true;
    }

    // END FUNCTION
     
    public function subfijo($xx)
    { // esta función regresa un subfijo para la cifra
		$xx = trim($xx);
		$xstrlen = strlen($xx);
		if ($xstrlen == 1 || $xstrlen == 2 || $xstrlen == 3)
		    $xsub = "";
		//
		if ($xstrlen == 4 || $xstrlen == 5 || $xstrlen == 6)
		    $xsub = "MIL";
		//
		return $xsub;
    }


    public function numero_letras($xcifra)
    {
		$xarray = [0 => "Cero",
		    1 => "UN", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE",
		    "DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE",
		    "VEINTI", 30 => "TREINTA", 40 => "CUARENTA", 50 => "CINCUENTA", 60 => "SESENTA", 70 => "SETENTA", 80 => "OCHENTA", 90 => "NOVENTA",
		    100 => "CIENTO", 200 => "DOSCIENTOS", 300 => "TRESCIENTOS", 400 => "CUATROCIENTOS", 500 => "QUINIENTOS", 600 => "SEISCIENTOS", 700 => "SETECIENTOS", 800 => "OCHOCIENTOS", 900 => "NOVECIENTOS"
		];
		$xcifra = trim($xcifra);
		$xlength = strlen($xcifra);
		$xpos_punto = strpos($xcifra, ".");
		$xaux_int = $xcifra;
		$xdecimales = "00";
		if (!($xpos_punto === false)) {
		    if ($xpos_punto == 0) {
			$xcifra = "0" . $xcifra;
			$xpos_punto = strpos($xcifra, ".");
		    }
		    $xaux_int = substr($xcifra, 0, $xpos_punto); // obtengo el entero de la cifra a covertir
		    $xdecimales = substr($xcifra . "00", $xpos_punto + 1, 2); // obtengo los valores decimales
		}
	     
		$XAUX = str_pad($xaux_int, 18, " ", STR_PAD_LEFT); // ajusto la longitud de la cifra, para que sea divisible por centenas de miles (grupos de 6)
		$xcadena = "";
		for ($xz = 0; $xz < 3; $xz++) {
		    $xaux = substr($XAUX, $xz * 6, 6);
		    $xi = 0;
		    $xlimite = 6; // inicializo el contador de centenas xi y establezco el límite a 6 dígitos en la parte entera
		    $xexit = true; // bandera para controlar el ciclo del While
		    while ($xexit) {
			if ($xi == $xlimite) { // si ya llegó al límite máximo de enteros
			    break; // termina el ciclo
			}
	     
			$x3digitos = ($xlimite - $xi) * -1; // comienzo con los tres primeros digitos de la cifra, comenzando por la izquierda
			$xaux = substr($xaux, $x3digitos, abs($x3digitos)); // obtengo la centena (los tres dígitos)
			for ($xy = 1; $xy < 4; $xy++) { // ciclo para revisar centenas, decenas y unidades, en ese orden
			    switch ($xy) {
				case 1: // checa las centenas
				    if (substr($xaux, 0, 3) < 100) { // si el grupo de tres dígitos es menor a una centena ( < 99) no hace nada y pasa a revisar las decenas
					 
				    } else {
					$key = (int) substr($xaux, 0, 3);
					if (TRUE === array_key_exists($key, $xarray)){  // busco si la centena es número redondo (100, 200, 300, 400, etc..)
					    $xseek = $xarray[$key];
					    $xsub = $this->subfijo($xaux); // devuelve el subfijo correspondiente (Millón, Millones, Mil o nada)
					    if (substr($xaux, 0, 3) == 100)
						$xcadena = " " . $xcadena . " CIEN " . $xsub;
					    else
						$xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
					    $xy = 3; // la centena fue redonda, entonces termino el ciclo del for y ya no reviso decenas ni unidades
					}
					else { // entra aquí si la centena no fue numero redondo (101, 253, 120, 980, etc.)
					    $key = (int) substr($xaux, 0, 1) * 100;
					    $xseek = $xarray[$key]; // toma el primer caracter de la centena y lo multiplica por cien y lo busca en el arreglo (para que busque 100,200,300, etc)
					    $xcadena = " " . $xcadena . " " . $xseek;
					} // ENDIF ($xseek)
				    } // ENDIF (substr($xaux, 0, 3) < 100)
				    break;
				case 2: // checa las decenas (con la misma lógica que las centenas)
				    if (substr($xaux, 1, 2) < 10) {
					 
				    } else {
					$key = (int) substr($xaux, 1, 2);
					if (TRUE === array_key_exists($key, $xarray)) {
					    $xseek = $xarray[$key];
					    $xsub = $this->subfijo($xaux);
					    if (substr($xaux, 1, 2) == 20)
						$xcadena = " " . $xcadena . " VEINTE " . $xsub;
					    else
						$xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
					    $xy = 3;
					}
					else {
					    $key = (int) substr($xaux, 1, 1) * 10;
					    $xseek = $xarray[$key];
					    if (20 == substr($xaux, 1, 1) * 10)
						$xcadena = " " . $xcadena . " " . $xseek;
					    else
						$xcadena = " " . $xcadena . " " . $xseek . " Y ";
					} // ENDIF ($xseek)
				    } // ENDIF (substr($xaux, 1, 2) < 10)
				    break;
				case 3: // checa las unidades
				    if (substr($xaux, 2, 1) < 1) { // si la unidad es cero, ya no hace nada
					 
				    } else {
					$key = (int) substr($xaux, 2, 1);
					$xseek = $xarray[$key]; // obtengo directamente el valor de la unidad (del uno al nueve)
					$xsub = $this->subfijo($xaux);
					$xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
				    } // ENDIF (substr($xaux, 2, 1) < 1)
				    break;
			    } // END SWITCH
			} // END FOR
			$xi = $xi + 3;
		    } // ENDDO
	     
		    if (substr(trim($xcadena), -5, 5) == "ILLON") // si la cadena obtenida termina en MILLON o BILLON, entonces le agrega al final la conjuncion DE
			$xcadena.= " DE";
	     
		    if (substr(trim($xcadena), -7, 7) == "ILLONES") // si la cadena obtenida en MILLONES o BILLONES, entoncea le agrega al final la conjuncion DE
			$xcadena.= " DE";
	     
		    // ----------- esta línea la puedes cambiar de acuerdo a tus necesidades o a tu país -------
		    if (trim($xaux) != "") {
			switch ($xz) {
			    case 0:
				if (trim(substr($XAUX, $xz * 6, 6)) == "1")
				    $xcadena.= "UN BILLON ";
				else
				    $xcadena.= " BILLONES ";
				break;
			    case 1:
				if (trim(substr($XAUX, $xz * 6, 6)) == "1")
				    $xcadena.= "UN MILLON ";
				else
				    $xcadena.= " MILLONES ";
				break;
			    case 2:
				if ($xcifra < 1) {
				    $xcadena = "CERO $xdecimales/100 SOLES";
				}
				if ($xcifra >= 1 && $xcifra < 2) {
				    $xcadena = "UN $xdecimales/100 SOLES";
				}
				if ($xcifra >= 2) {
				    $xcadena.= " $xdecimales/100 SOLES"; //
				}
				break;
			} // endswitch ($xz)
		    } // ENDIF (trim($xaux) != "")
		    // ------------------      en este caso, para México se usa esta leyenda     ----------------
		    $xcadena = str_replace("VEINTI ", "VEINTI", $xcadena); // quito el espacio para el VEINTI, para que quede: VEINTICUATRO, VEINTIUN, VEINTIDOS, etc
		    $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
		    $xcadena = str_replace("UN UN", "UN", $xcadena); // quito la duplicidad
		    $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
		    $xcadena = str_replace("BILLON DE MILLONES", "BILLON DE", $xcadena); // corrigo la leyenda
		    $xcadena = str_replace("BILLONES DE MILLONES", "BILLONES DE", $xcadena); // corrigo la leyenda
		    $xcadena = str_replace("DE UN", "UN", $xcadena); // corrigo la leyenda
		} // ENDFOR ($xz)
		return trim($xcadena);
    }

    public function DescripcionMes($mes=null)
    {
        setlocale (LC_TIME,"spanish");
        //$mes = 01;
        $mes=$mes-1;
        $fechaInicio = '2016-05-01';
        $nuevafecha = strtotime ( '+'.$mes.' month' , strtotime ( $fechaInicio ) ) ;
        $nuevafecha = strtoupper(strftime ( '%b %Y' , $nuevafecha ) ) ;
        return $nuevafecha;
    }

}
