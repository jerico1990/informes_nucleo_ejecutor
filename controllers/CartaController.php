<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Settings;
use yii\helpers\Json;


class CartaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public $web_url_servicio = "http://fspreset.minagri.gob.pe:5000";
    //public $web_url_servicio = "http://si2.minagri.gob.pe:4000";
    
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($nucleo=null)
    {
        date_default_timezone_set('America/Lima');
        setlocale (LC_TIME,"spanish");
        $phpWord = new PhpWord();
        $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/formatos/CARTA_AUTORIZACION_NE.docx');
        
        $json_cabecera = [];
        $json_detalle_actividades = [];

        $txt_ciudad = "";
        $txt_fecha_carta = strftime("%d de %B del %Y", strtotime(date('d-m-Y')));
        $txt_nombre_nucleo = "";
        $txt_cuenta_ahorros = "";
        $txt_fecha_convenio = strftime("%d de %B del %Y", strtotime(date('d-m-Y')));
        $txt_apenom_pre = "";
        $cod_dnipre = "";
        $firma_pre = "LogoMinagri.jpg";


        /* Seteando servicio de cabecera del nucleo ejecutor */

        $curl_cabecera = curl_init();
        curl_setopt($curl_cabecera, CURLOPT_URL,$this->web_url_servicio."/nue_lst_rp02");
        curl_setopt($curl_cabecera, CURLOPT_POST, TRUE);
        curl_setopt($curl_cabecera, CURLOPT_POSTFIELDS, "nucleo=".$nucleo."");
        curl_setopt($curl_cabecera, CURLOPT_RETURNTRANSFER, true);
        $remote_server_cabecera = curl_exec ($curl_cabecera);
        $json_cabecera_validar = Json::decode($json = $remote_server_cabecera);
        if(count($json_cabecera_validar)==0){
            return json_encode(['msg'=>'No encontrado','flg'=>0]);
            //var_dump($json_cabecera_validar);die;
        }
        
        if($remote_server_cabecera){
            $json_cabecera = Json::decode($json = $remote_server_cabecera);

            $txt_nombre_nucleo = $json_cabecera[0]["TXT_NOMBRE_NUCLEO"];
            /*
            $txt_apenom_pre = $json_cabecera[0]["TXT_NOMPRE"]." ".$json_cabecera[0]["TXT_APEPRE"];
            $cod_dnipre = $json_cabecera[0]["COD_DNIPRE"];*/
           
        }
        curl_close ($curl_cabecera);
        /*
        $template->setValue('TXT_CIUDAD', $txt_ciudad);
        $template->setValue('TXT_FECHA_CARTA', $txt_fecha_carta);*/
        $template->setValue('TXT_NOMBRE_NUCLEO', $txt_nombre_nucleo);
        /*
        $template->setValue('TXT_CUENTA_AHORROS', $txt_cuenta_ahorros);
        $template->setValue('TXT_FECHA_CONVENIO', $txt_fecha_convenio);
        $template->setValue('TXT_APENOM_PRE', $txt_apenom_pre);
        $template->setValue('COD_DNIPRE', $cod_dnipre);*/


        #reemplaza una variable con una imagen
        
        //$template->setImageValue( 'FIRMA_PRE',array('path' =>\Yii::getAlias('@webroot').'/img/LogoMinagri.jpg', 'width' => 100, 'height' => 100, 'ratio' => false)  );
        //$template->setImageValue( 'FIRMA_PRE',array('path' =>\Yii::getAlias('@webroot').'/img/'.$firma_pre)  );

        
        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $template->saveAs($temp_file);
        header("Content-Disposition: attachment; filename=CARTA-AUTORIZACION.docx");
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file);  // remove temp file
        return true;
    }
    /*
    public function setImageValue($search, $replace)
    {
        // Sanity check
        if (!file_exists($replace))
        {
            return;
        }

        // Delete current image
        $this->zipClass->deleteName(\Yii::$app->request->BaseUrl.'/web/img/' . $search);

        // Add a new one
        $this->zipClass->addFile($replace, \Yii::$app->request->BaseUrl.'/web/img/' . $search);
    }*/
}
