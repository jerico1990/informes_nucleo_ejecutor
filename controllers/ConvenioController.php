<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Settings;
use yii\helpers\Json;


class ConvenioController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */

    public $web_url_servicio = "http://fspreset.minagri.gob.pe:5000";
    //public $web_url_servicio = "http://si2.minagri.gob.pe:4000";

    public function actionIndex($nucleo=null)
    {
        date_default_timezone_set('America/Lima');
        setlocale(LC_TIME,"spanish");
        setlocale(LC_CTYPE, 'es_ES.UTF8');
        $phpWord = new PhpWord();
        $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/formatos/CONVENIO.docx');
        
        $json_cabecera = [];
        $json_detalle_actividades = [];

        $txt_apenom_vice = "";
        $txt_dnivice = "";
        $txt_rm = "";
        $txt_rm_fecha = strftime("%d de %B del %Y", strtotime(date('d-m-Y')));
        $txt_nombre_nucleo = "";
        $txt_apenom_pre = "";
        $cod_dnipre = "";
        $txt_apenom_sec = "";
        $cod_dnisec = "";
        $txt_apenom_tes = "";
        $cod_dnites = "";
        $txt_domicilio_ne = "";
        $txt_region_ne = "";
        $txt_provincia_ne = "";
        $txt_distrito_ne = "";
        $fec_acta_constitucion_ne = strftime("%d de %B del %Y", strtotime(date('d-m-Y')));
        $monto_total = 0.00;
        $fec_suscripcion = strftime("%d de %B del %Y", strtotime(date('d-m-Y')));


        /* Seteando servicio de cabecera del nucleo ejecutor */

        $curl_cabecera = curl_init();
        curl_setopt($curl_cabecera, CURLOPT_URL,$this->web_url_servicio."/nue_lst_rp02");
        curl_setopt($curl_cabecera, CURLOPT_POST, TRUE);
        curl_setopt($curl_cabecera, CURLOPT_POSTFIELDS, "nucleo=".$nucleo."");
        curl_setopt($curl_cabecera, CURLOPT_RETURNTRANSFER, true);
        $remote_server_cabecera = curl_exec ($curl_cabecera);
        $json_cabecera_validar = Json::decode($json = $remote_server_cabecera);
        if(count($json_cabecera_validar)==0){
            return json_encode(['msg'=>'No encontrado','flg'=>0]);
            //var_dump($json_cabecera_validar);die;
        }

        if($remote_server_cabecera){
            $json_cabecera = Json::decode($json = $remote_server_cabecera);

            $txt_apenom_vice = $json_cabecera[0]["TXT_APENOM_VICE"];
            $txt_dnivice = $json_cabecera[0]["TXT_DNIVICE"];
            $txt_nombre_nucleo = $json_cabecera[0]["TXT_NOMBRE_NUCLEO"];
            $txt_apenom_pre = $json_cabecera[0]["TXT_APEPRE"]." ".$json_cabecera[0]["TXT_NOMPRE"];
            $cod_dnipre = $json_cabecera[0]["COD_DNIPRE"];
            $txt_apenom_sec = $json_cabecera[0]["TXT_APESEC"]." ".$json_cabecera[0]["TXT_NOMSEC"];
            $cod_dnisec = $json_cabecera[0]["COD_DNISEC"];
            $txt_apenom_tes = $json_cabecera[0]["TXT_APETES"]." ".$json_cabecera[0]["TXT_NOMTES"];
            $cod_dnites = $json_cabecera[0]["COD_DNITES"];

            $txt_domicilio_ne = $json_cabecera[0]["TXT_DOMNUCLEO"];
            $txt_region_ne = $json_cabecera[0]["DPTO"];
            $txt_provincia_ne = $json_cabecera[0]["PROV"];
            $txt_distrito_ne = $json_cabecera[0]["DIST"];

            if($json_cabecera[0]["TXT_FECNUCLEO"]){
                $fec_acta_constitucion_ne = str_replace("/","-",$json_cabecera[0]["TXT_FECNUCLEO"]);
                $fec_acta_constitucion_ne = strftime("%d de %B del %Y", strtotime($fec_acta_constitucion_ne));
            }else{
                $fec_acta_constitucion_ne = "";
            }

            $fec_suscripcion = $json_cabecera[0]["FEC_SUSCRIPCION"];
            if($fec_suscripcion){
                $fec_suscripcion = strftime("%d días del mes de %B del año %Y", strtotime($fec_suscripcion));
            }else{
                $fec_suscripcion = "";
            }
            
        }
        curl_close ($curl_cabecera);

        /* Seteando servicio de actividades */

        $curl_detalle_actividades = curl_init();
        curl_setopt($curl_detalle_actividades, CURLOPT_URL,$this->web_url_servicio."/nue_lst_rp03");
        curl_setopt($curl_detalle_actividades, CURLOPT_POST, TRUE);
        curl_setopt($curl_detalle_actividades, CURLOPT_POSTFIELDS, "nucleo=".$nucleo."");
        curl_setopt($curl_detalle_actividades, CURLOPT_RETURNTRANSFER, true);
        $remote_server_detalle_actividades = curl_exec ($curl_detalle_actividades);

        if($remote_server_detalle_actividades){
            $json_detalle_actividades = Json::decode($json = $remote_server_detalle_actividades);
        }


        $template->setValue('TXT_APENOM_VICE', $txt_apenom_vice);
        $template->setValue('TXT_DNIVICE', $txt_dnivice);
        $template->setValue('TXT_NOMBRE_NUCLEO_M', mb_strtoupper($txt_nombre_nucleo,'utf-8'));
        $template->setValue('TXT_NOMBRE_NUCLEO', $txt_nombre_nucleo);
        $template->setValue('TXT_APENOM_PRE', $txt_apenom_pre);
        $template->setValue('COD_DNIPRE', $cod_dnipre);
        $template->setValue('TXT_APENOM_SEC', $txt_apenom_sec);
        $template->setValue('COD_DNISEC', $cod_dnisec);
        $template->setValue('TXT_APENOM_TES', $txt_apenom_tes);
        $template->setValue('COD_DNITES', $cod_dnites);
        $template->setValue('TXT_DOMICILIO_NE', mb_strtoupper($txt_domicilio_ne,'utf-8'));

        $template->setValue('TXT_DISTRITO_NE', ucwords($txt_distrito_ne));
        $template->setValue('TXT_PROVINCIA_NE', ucwords($txt_provincia_ne));
        $template->setValue('TXT_DEPARTAMENTO_NE', ucwords($txt_region_ne));

        $template->setValue('FEC_ACTA_CONSTITUCION_NE', $fec_acta_constitucion_ne);
        $template->setValue('FEC_SUSCRIPCION', $fec_suscripcion);

        
        $monto_total = 0 ;
        $monto_elaboracion_total = 0 ;
        $monto_maximo_total = 0 ;
        if(count($json_detalle_actividades)>0){
            $template->cloneRow('NRO', count($json_detalle_actividades));
            $rows = 1 ;
            
            foreach($json_detalle_actividades as $actividad){
                $monto_maximo=$actividad['IMP_PRESUPUESTO'];
                $monto_elaboracion = ($monto_maximo*0.0035);

                

                $template->setValue('NRO#'.$rows.'', "".ucwords($rows)."");
                $template->setValue('COD_AMCRD#'.$rows.'', "".ucwords($actividad['COD_AMCRD'])."");
                $template->setValue('TXT_ACTIVIDAD#'.$rows.'', "".mb_strtoupper($actividad['TXT_ACTIVIDAD'],'utf-8')."");
                $template->setValue('MONTO_MAXIMO#'.$rows.'', "".ucwords(number_format($monto_maximo, 2, '.', ','))."");
                $template->setValue('MONTO_ELABORACION#'.$rows.'', "".ucwords(number_format($monto_elaboracion, 2, '.', ','))."");

                $monto_maximo_total = $monto_maximo_total + $monto_maximo;
                $monto_elaboracion_total = $monto_elaboracion_total + $monto_elaboracion;
                
                $rows++;
            }
        }else{
            $template->setValue('NRO', '');
            $template->setValue('COD_AMCRD', '');
            $template->setValue('TXT_ACTIVIDAD', '');
            $template->setValue('MONTO_MAXIMO', '');
            $template->setValue('MONTO_ELABORACION', '');
        }

        
        $monto_total = round($monto_maximo_total,0) + round($monto_elaboracion_total,0);

        $template->setValue('MONTO_MAXIMO_TOTAL', ucwords(number_format(round($monto_maximo_total,0), 2, '.', ',')));
        $template->setValue('MONTO_ELABORACION_TOTAL', ucwords(number_format(round($monto_elaboracion_total,0), 2, '.', ',')));
        $template->setValue('MONTO_TOTAL', ucwords(number_format($monto_total, 2, '.', ',')));

        $template->setValue('MONTO_TOTAL_', "S/ ".ucwords(number_format($monto_total, 2, '.', ',')));
        $template->setValue('MONTO_TOTAL_TEXTO', ucfirst(strtolower($this->numero_letras($monto_total))));
        
        //$template->setImageValue( 'FIRMA_PRE',array('path' =>'http://fspreset.minagri.gob.pe:3000/imagen/nucleos/f-122-1', 'width' => 70, 'height' => 50, 'ratio' => true)  );

        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $template->saveAs($temp_file);
        header("Content-Disposition: attachment; filename=CONVENIO.docx");
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file);  // remove temp file
        return true;
    }


    public function actionIndexFirma($nucleo=null)
    {
        date_default_timezone_set('America/Lima');
        setlocale(LC_TIME,"spanish");
        setlocale(LC_CTYPE, 'es_ES.UTF8');
        $phpWord = new PhpWord();
        $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/formatos/CONVENIO-FIRMADO.docx');
        
        $json_cabecera = [];
        $json_detalle_actividades = [];

        $txt_apenom_vice = "";
        $txt_dnivice = "";
        $txt_rm = "";
        $txt_rm_fecha = strftime("%d de %B del %Y", strtotime(date('d-m-Y')));
        $txt_nombre_nucleo = "";
        $txt_apenom_pre = "";
        $cod_dnipre = "";
        $txt_apenom_sec = "";
        $cod_dnisec = "";
        $txt_apenom_tes = "";
        $cod_dnites = "";
        $txt_domicilio_ne = "";
        $txt_region_ne = "";
        $txt_provincia_ne = "";
        $txt_distrito_ne = "";
        $fec_acta_constitucion_ne = strftime("%d de %B del %Y", strtotime(date('d-m-Y')));
        $fec_suscripcion = strftime("%d de %B del %Y", strtotime(date('d-m-Y')));
        $monto_total = 0.00;


        /* Seteando servicio de cabecera del nucleo ejecutor */

        $curl_cabecera = curl_init();
        curl_setopt($curl_cabecera, CURLOPT_URL,$this->web_url_servicio."/nue_lst_rp02");
        curl_setopt($curl_cabecera, CURLOPT_POST, TRUE);
        curl_setopt($curl_cabecera, CURLOPT_POSTFIELDS, "nucleo=".$nucleo."");
        curl_setopt($curl_cabecera, CURLOPT_RETURNTRANSFER, true);
        $remote_server_cabecera = curl_exec ($curl_cabecera);
        $json_cabecera_validar = Json::decode($json = $remote_server_cabecera);
        if(count($json_cabecera_validar)==0){
            return json_encode(['msg'=>'No encontrado','flg'=>0]);
            //var_dump($json_cabecera_validar);die;
        }

        if($remote_server_cabecera){
            $json_cabecera = Json::decode($json = $remote_server_cabecera);

            $txt_apenom_vice = $json_cabecera[0]["TXT_APENOM_VICE"];
            $txt_dnivice = $json_cabecera[0]["TXT_DNIVICE"];
            $txt_nombre_nucleo = $json_cabecera[0]["TXT_NOMBRE_NUCLEO"];
            $txt_apenom_pre = $json_cabecera[0]["TXT_APEPRE"]." ".$json_cabecera[0]["TXT_NOMPRE"];
            $cod_dnipre = $json_cabecera[0]["COD_DNIPRE"];
            $txt_apenom_sec = $json_cabecera[0]["TXT_APESEC"]." ".$json_cabecera[0]["TXT_NOMSEC"];
            $cod_dnisec = $json_cabecera[0]["COD_DNISEC"];
            $txt_apenom_tes = $json_cabecera[0]["TXT_APETES"]." ".$json_cabecera[0]["TXT_NOMTES"];
            $cod_dnites = $json_cabecera[0]["COD_DNITES"];

            $txt_domicilio_ne = $json_cabecera[0]["TXT_DOMNUCLEO"];
            $txt_region_ne = $json_cabecera[0]["DPTO"];
            $txt_provincia_ne = $json_cabecera[0]["PROV"];
            $txt_distrito_ne = $json_cabecera[0]["DIST"];

            if($json_cabecera[0]["TXT_FECNUCLEO"]){
                $fec_acta_constitucion_ne = str_replace("/","-",$json_cabecera[0]["TXT_FECNUCLEO"]);
                $fec_acta_constitucion_ne = strftime("%d de %B del %Y", strtotime($fec_acta_constitucion_ne));
            }else{
                $fec_acta_constitucion_ne = "";
            }

            $fec_suscripcion = $json_cabecera[0]["FEC_SUSCRIPCION"];
            if($fec_suscripcion){
                $fec_suscripcion = strftime("%d días del mes de %B del año %Y", strtotime($fec_suscripcion));
            }else{
                $fec_suscripcion = "";
            }
            
        }
        curl_close ($curl_cabecera);

        /* Seteando servicio de actividades */

        $curl_detalle_actividades = curl_init();
        curl_setopt($curl_detalle_actividades, CURLOPT_URL,$this->web_url_servicio."/nue_lst_rp03");
        curl_setopt($curl_detalle_actividades, CURLOPT_POST, TRUE);
        curl_setopt($curl_detalle_actividades, CURLOPT_POSTFIELDS, "nucleo=".$nucleo."");
        curl_setopt($curl_detalle_actividades, CURLOPT_RETURNTRANSFER, true);
        $remote_server_detalle_actividades = curl_exec ($curl_detalle_actividades);

        if($remote_server_detalle_actividades){
            $json_detalle_actividades = Json::decode($json = $remote_server_detalle_actividades);
        }


        $template->setValue('TXT_APENOM_VICE', $txt_apenom_vice);
        $template->setValue('TXT_DNIVICE', $txt_dnivice);
        $template->setValue('TXT_NOMBRE_NUCLEO_M', mb_strtoupper($txt_nombre_nucleo,'utf-8'));
        $template->setValue('TXT_NOMBRE_NUCLEO', $txt_nombre_nucleo);
        $template->setValue('TXT_APENOM_PRE', $txt_apenom_pre);
        $template->setValue('COD_DNIPRE', $cod_dnipre);
        $template->setValue('TXT_APENOM_SEC', $txt_apenom_sec);
        $template->setValue('COD_DNISEC', $cod_dnisec);
        $template->setValue('TXT_APENOM_TES', $txt_apenom_tes);
        $template->setValue('COD_DNITES', $cod_dnites);
        $template->setValue('TXT_DOMICILIO_NE', mb_strtoupper($txt_domicilio_ne,'utf-8'));
        $template->setValue('TXT_DISTRITO_NE', ucwords($txt_distrito_ne));
        $template->setValue('TXT_PROVINCIA_NE', ucwords($txt_provincia_ne));
        $template->setValue('TXT_DEPARTAMENTO_NE', ucwords($txt_region_ne));
        $template->setValue('FEC_ACTA_CONSTITUCION_NE', $fec_acta_constitucion_ne);
        
        $template->setValue('FEC_SUSCRIPCION', $fec_suscripcion);

        
        $monto_total = 0 ;
        $monto_elaboracion_total = 0 ;
        $monto_maximo_total = 0 ;
        if(count($json_detalle_actividades)>0){
            $template->cloneRow('NRO', count($json_detalle_actividades));
            $rows = 1 ;
            
            foreach($json_detalle_actividades as $actividad){
                $monto_maximo=$actividad['IMP_PRESUPUESTO'];
                $monto_elaboracion = ($monto_maximo*0.0035);

                

                $template->setValue('NRO#'.$rows.'', "".ucwords($rows)."");
                $template->setValue('COD_AMCRD#'.$rows.'', "".ucwords($actividad['COD_AMCRD'])."");
                $template->setValue('TXT_ACTIVIDAD#'.$rows.'', "".mb_strtoupper($actividad['TXT_ACTIVIDAD'],'utf-8')."");
                $template->setValue('MONTO_MAXIMO#'.$rows.'', "".ucwords(number_format($monto_maximo, 2, '.', ','))."");
                $template->setValue('MONTO_ELABORACION#'.$rows.'', "".ucwords(number_format($monto_elaboracion, 2, '.', ','))."");

                $monto_maximo_total = $monto_maximo_total + $monto_maximo;
                $monto_elaboracion_total = $monto_elaboracion_total + $monto_elaboracion;
                
                $rows++;
            }
        }else{
            $template->setValue('NRO', '');
            $template->setValue('COD_AMCRD', '');
            $template->setValue('TXT_ACTIVIDAD', '');
            $template->setValue('MONTO_MAXIMO', '');
            $template->setValue('MONTO_ELABORACION', '');
        }

        
        $monto_total = round($monto_maximo_total,0) + round($monto_elaboracion_total,0);

        $template->setValue('MONTO_MAXIMO_TOTAL', ucwords(number_format(round($monto_maximo_total,0), 2, '.', ',')));
        $template->setValue('MONTO_ELABORACION_TOTAL', ucwords(number_format(round($monto_elaboracion_total,0), 2, '.', ',')));
        $template->setValue('MONTO_TOTAL', ucwords(number_format($monto_total, 2, '.', ',')));

        $template->setValue('MONTO_TOTAL_', "S/ ".ucwords(number_format($monto_total, 2, '.', ',')));
        $template->setValue('MONTO_TOTAL_TEXTO', ucfirst(strtolower($this->numero_letras($monto_total))));
        


        $curl_cabecera_firma_1 = curl_init();
        curl_setopt($curl_cabecera_firma_1, CURLOPT_URL,'http://fspreset.minagri.gob.pe:3000/imagen/nucleos/f-'.$nucleo.'-1');
        curl_setopt($curl_cabecera_firma_1, CURLOPT_RETURNTRANSFER, true);
        $remote_server_cabecera_firma_1 = curl_exec ($curl_cabecera_firma_1);
        $json_cabecera_validar_firma_1 = json_decode($remote_server_cabecera_firma_1);
        if($json_cabecera_validar_firma_1==NULL){
            $template->setImageValue('FIRMA_PRE',array('path' =>'http://fspreset.minagri.gob.pe:3000/imagen/nucleos/f-'.$nucleo.'-1', 'width' => 70, 'height' => 50, 'ratio' => true)  );
            $template->setImageValue('FIRMA_PRE_F',array('path' =>'http://fspreset.minagri.gob.pe:3000/imagen/nucleos/f-'.$nucleo.'-1', 'width' => 100, 'height' => 80, 'ratio' => true)  );
        }else{
            $template->setValue('FIRMA_PRE', 'No hay firma');
            $template->setValue('FIRMA_PRE_F', 'No hay firma');
        }

        $curl_cabecera_firma_2 = curl_init();
        curl_setopt($curl_cabecera_firma_2, CURLOPT_URL,'http://fspreset.minagri.gob.pe:3000/imagen/nucleos/f-'.$nucleo.'-2');
        curl_setopt($curl_cabecera_firma_2, CURLOPT_RETURNTRANSFER, true);
        $remote_server_cabecera_firma_2 = curl_exec ($curl_cabecera_firma_2);
        $json_cabecera_validar_firma_2 = json_decode($remote_server_cabecera_firma_2);
        if($json_cabecera_validar_firma_2==NULL){
            $template->setImageValue('FIRMA_TES',array('path' =>'http://fspreset.minagri.gob.pe:3000/imagen/nucleos/f-'.$nucleo.'-2', 'width' => 70, 'height' => 50, 'ratio' => true)  );
            $template->setImageValue('FIRMA_TES_F',array('path' =>'http://fspreset.minagri.gob.pe:3000/imagen/nucleos/f-'.$nucleo.'-2', 'width' => 100, 'height' => 80, 'ratio' => true)  );
        }else{
            $template->setValue('FIRMA_TES', 'No hay firma');
            $template->setValue('FIRMA_TES_F', 'No hay firma');
        }

        $curl_cabecera_firma_3 = curl_init();
        curl_setopt($curl_cabecera_firma_3, CURLOPT_URL,'http://fspreset.minagri.gob.pe:3000/imagen/nucleos/f-'.$nucleo.'-3');
        curl_setopt($curl_cabecera_firma_3, CURLOPT_RETURNTRANSFER, true);
        $remote_server_cabecera_firma_3 = curl_exec ($curl_cabecera_firma_3);
        $json_cabecera_validar_firma_3 = json_decode($remote_server_cabecera_firma_3);
        if($json_cabecera_validar_firma_3==NULL){
            $template->setImageValue('FIRMA_SEC',array('path' =>'http://fspreset.minagri.gob.pe:3000/imagen/nucleos/f-'.$nucleo.'-3', 'width' => 70, 'height' => 50, 'ratio' => true)  );
            $template->setImageValue('FIRMA_SEC_F',array('path' =>'http://fspreset.minagri.gob.pe:3000/imagen/nucleos/f-'.$nucleo.'-3', 'width' => 100, 'height' => 80, 'ratio' => true)  );
        }else{
            $template->setValue('FIRMA_SEC', 'No hay firma');
            $template->setValue('FIRMA_SEC_F', 'No hay firma');
        }

        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $template->saveAs($temp_file);
        header("Content-Disposition: attachment; filename=CONVENIO-FIRMADO.docx");
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file);  // remove temp file
        return true;
    }



    public function subfijo($xx)
    { // esta función regresa un subfijo para la cifra
		$xx = trim($xx);
		$xstrlen = strlen($xx);
		if ($xstrlen == 1 || $xstrlen == 2 || $xstrlen == 3)
		    $xsub = "";
		//
		if ($xstrlen == 4 || $xstrlen == 5 || $xstrlen == 6)
		    $xsub = "MIL";
		//
		return $xsub;
    }


    public function numero_letras($xcifra)
    {
		$xarray = [0 => "Cero",
		    1 => "UN", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE",
		    "DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE",
		    "VEINTI", 30 => "TREINTA", 40 => "CUARENTA", 50 => "CINCUENTA", 60 => "SESENTA", 70 => "SETENTA", 80 => "OCHENTA", 90 => "NOVENTA",
		    100 => "CIENTO", 200 => "DOSCIENTOS", 300 => "TRESCIENTOS", 400 => "CUATROCIENTOS", 500 => "QUINIENTOS", 600 => "SEISCIENTOS", 700 => "SETECIENTOS", 800 => "OCHOCIENTOS", 900 => "NOVECIENTOS"
		];
		$xcifra = trim($xcifra);
		$xlength = strlen($xcifra);
		$xpos_punto = strpos($xcifra, ".");
		$xaux_int = $xcifra;
		$xdecimales = "00";
		if (!($xpos_punto === false)) {
		    if ($xpos_punto == 0) {
			$xcifra = "0" . $xcifra;
			$xpos_punto = strpos($xcifra, ".");
		    }
		    $xaux_int = substr($xcifra, 0, $xpos_punto); // obtengo el entero de la cifra a covertir
		    $xdecimales = substr($xcifra . "00", $xpos_punto + 1, 2); // obtengo los valores decimales
		}
	     
		$XAUX = str_pad($xaux_int, 18, " ", STR_PAD_LEFT); // ajusto la longitud de la cifra, para que sea divisible por centenas de miles (grupos de 6)
		$xcadena = "";
		for ($xz = 0; $xz < 3; $xz++) {
		    $xaux = substr($XAUX, $xz * 6, 6);
		    $xi = 0;
		    $xlimite = 6; // inicializo el contador de centenas xi y establezco el límite a 6 dígitos en la parte entera
		    $xexit = true; // bandera para controlar el ciclo del While
		    while ($xexit) {
			if ($xi == $xlimite) { // si ya llegó al límite máximo de enteros
			    break; // termina el ciclo
			}
	     
			$x3digitos = ($xlimite - $xi) * -1; // comienzo con los tres primeros digitos de la cifra, comenzando por la izquierda
			$xaux = substr($xaux, $x3digitos, abs($x3digitos)); // obtengo la centena (los tres dígitos)
			for ($xy = 1; $xy < 4; $xy++) { // ciclo para revisar centenas, decenas y unidades, en ese orden
			    switch ($xy) {
				case 1: // checa las centenas
				    if (substr($xaux, 0, 3) < 100) { // si el grupo de tres dígitos es menor a una centena ( < 99) no hace nada y pasa a revisar las decenas
					 
				    } else {
					$key = (int) substr($xaux, 0, 3);
					if (TRUE === array_key_exists($key, $xarray)){  // busco si la centena es número redondo (100, 200, 300, 400, etc..)
					    $xseek = $xarray[$key];
					    $xsub = $this->subfijo($xaux); // devuelve el subfijo correspondiente (Millón, Millones, Mil o nada)
					    if (substr($xaux, 0, 3) == 100)
						$xcadena = " " . $xcadena . " CIEN " . $xsub;
					    else
						$xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
					    $xy = 3; // la centena fue redonda, entonces termino el ciclo del for y ya no reviso decenas ni unidades
					}
					else { // entra aquí si la centena no fue numero redondo (101, 253, 120, 980, etc.)
					    $key = (int) substr($xaux, 0, 1) * 100;
					    $xseek = $xarray[$key]; // toma el primer caracter de la centena y lo multiplica por cien y lo busca en el arreglo (para que busque 100,200,300, etc)
					    $xcadena = " " . $xcadena . " " . $xseek;
					} // ENDIF ($xseek)
				    } // ENDIF (substr($xaux, 0, 3) < 100)
				    break;
				case 2: // checa las decenas (con la misma lógica que las centenas)
				    if (substr($xaux, 1, 2) < 10) {
					 
				    } else {
					$key = (int) substr($xaux, 1, 2);
					if (TRUE === array_key_exists($key, $xarray)) {
					    $xseek = $xarray[$key];
					    $xsub = $this->subfijo($xaux);
					    if (substr($xaux, 1, 2) == 20)
						$xcadena = " " . $xcadena . " VEINTE " . $xsub;
					    else
						$xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
					    $xy = 3;
					}
					else {
					    $key = (int) substr($xaux, 1, 1) * 10;
					    $xseek = $xarray[$key];
					    if (20 == substr($xaux, 1, 1) * 10)
						$xcadena = " " . $xcadena . " " . $xseek;
					    else
						$xcadena = " " . $xcadena . " " . $xseek . " Y ";
					} // ENDIF ($xseek)
				    } // ENDIF (substr($xaux, 1, 2) < 10)
				    break;
				case 3: // checa las unidades
				    if (substr($xaux, 2, 1) < 1) { // si la unidad es cero, ya no hace nada
					 
				    } else {
					$key = (int) substr($xaux, 2, 1);
					$xseek = $xarray[$key]; // obtengo directamente el valor de la unidad (del uno al nueve)
					$xsub = $this->subfijo($xaux);
					$xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
				    } // ENDIF (substr($xaux, 2, 1) < 1)
				    break;
			    } // END SWITCH
			} // END FOR
			$xi = $xi + 3;
		    } // ENDDO
	     
		    if (substr(trim($xcadena), -5, 5) == "ILLON") // si la cadena obtenida termina en MILLON o BILLON, entonces le agrega al final la conjuncion DE
			$xcadena.= " DE";
	     
		    if (substr(trim($xcadena), -7, 7) == "ILLONES") // si la cadena obtenida en MILLONES o BILLONES, entoncea le agrega al final la conjuncion DE
			$xcadena.= " DE";
	     
		    // ----------- esta línea la puedes cambiar de acuerdo a tus necesidades o a tu país -------
		    if (trim($xaux) != "") {
			switch ($xz) {
			    case 0:
				if (trim(substr($XAUX, $xz * 6, 6)) == "1")
				    $xcadena.= "UN BILLON ";
				else
				    $xcadena.= " BILLONES ";
				break;
			    case 1:
				if (trim(substr($XAUX, $xz * 6, 6)) == "1")
				    $xcadena.= "UN MILLON ";
				else
				    $xcadena.= " MILLONES ";
				break;
			    case 2:
				if ($xcifra < 1) {
				    $xcadena = "CON CERO $xdecimales/100 SOLES";
				}
				if ($xcifra >= 1 && $xcifra < 2) {
				    $xcadena = " CON UN $xdecimales/100 SOLES";
				}
				if ($xcifra >= 2) {
				    $xcadena.= "CON $xdecimales/100 SOLES"; //
				}
				break;
			} // endswitch ($xz)
		    } // ENDIF (trim($xaux) != "")
		    // ------------------      en este caso, para México se usa esta leyenda     ----------------
		    $xcadena = str_replace("VEINTI ", "VEINTI", $xcadena); // quito el espacio para el VEINTI, para que quede: VEINTICUATRO, VEINTIUN, VEINTIDOS, etc
		    $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
		    $xcadena = str_replace("UN UN", "UN", $xcadena); // quito la duplicidad
		    $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
		    $xcadena = str_replace("BILLON DE MILLONES", "BILLON DE", $xcadena); // corrigo la leyenda
		    $xcadena = str_replace("BILLONES DE MILLONES", "BILLONES DE", $xcadena); // corrigo la leyenda
		    $xcadena = str_replace("DE UN", "UN", $xcadena); // corrigo la leyenda
		} // ENDFOR ($xz)
		return trim($xcadena);
    }

    public function DescripcionMes($mes=null)
    {
        setlocale (LC_TIME,"spanish");
        //$mes = 01;
        $mes=$mes-1;
        $fechaInicio = '2016-05-01';
        $nuevafecha = strtotime ( '+'.$mes.' month' , strtotime ( $fechaInicio ) ) ;
        $nuevafecha = strtoupper(strftime ( '%b %Y' , $nuevafecha ) ) ;
        return $nuevafecha;
    }
}

