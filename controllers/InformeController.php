<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Settings;
use yii\helpers\Json;


class InformeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public $web_url_servicio = "http://fspreset.minagri.gob.pe:5000";
    //public $web_url_servicio = "http://si2.minagri.gob.pe:4000";
    
    public function actionIndex($nucleo=null)
    {
        
        date_default_timezone_set('America/Lima');
        setlocale (LC_TIME,"spanish");

        $phpWord = new PhpWord();
        $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/formatos/INFORME_TECNICO_DGIAR.docx');
        
        $json_cabecera = [];
        $json_detalle_actividades = [];
        $nucleo_ejecutor = "";
        $convenio = "";
        $fecha_ingreso_sistema = strftime("%d de %B del %Y", strtotime(date('d-m-Y')));
        $fecha_elaboracion_informe = strftime("%d de %B del %Y", strtotime(date('d-m-Y')));
        $fecha_ingreso_sistema_carta = strftime("%d de %B del %Y", strtotime(date('d-m-Y')));
        $fecha_ingreso_sistema_hora = strftime("%I:%M del %d de %B del %Y", strtotime(date('d-m-Y H:i:s')));
        $prefijo_hora = "";
        //$memorando = "276";
        $entidad_responsable = "";

        /* Seteando servicio de cabecera del nucleo ejecutor */

        $curl_cabecera = curl_init();
        curl_setopt($curl_cabecera, CURLOPT_URL,$this->web_url_servicio."/nue_lst_rp02");
        curl_setopt($curl_cabecera, CURLOPT_POST, TRUE);
        curl_setopt($curl_cabecera, CURLOPT_POSTFIELDS, "nucleo=".$nucleo."");
        curl_setopt($curl_cabecera, CURLOPT_RETURNTRANSFER, true);
        $remote_server_cabecera = curl_exec ($curl_cabecera);
        $json_cabecera_validar = Json::decode($json = $remote_server_cabecera);
        if(count($json_cabecera_validar)==0){
            return json_encode(['msg'=>'No encontrado','flg'=>0]);
            //var_dump($json_cabecera_validar);die;
        }
        

        if($remote_server_cabecera){
            $json_cabecera = Json::decode($json = $remote_server_cabecera);
            $nucleo_ejecutor = $json_cabecera[0]["TXT_NOMBRE_NUCLEO"];
            $convenio = $json_cabecera[0]["TXT_NROCONVENIO"];
            $fecha_ingreso_sistema = ($json_cabecera[0]["FEC_ENVTRA"])?strftime("%d de %B del %Y", strtotime($json_cabecera[0]["FEC_ENVTRA"])):$fecha_ingreso_sistema;
            $fecha_elaboracion_informe = ($json_cabecera[0]["FEC_REGISTRO"])?strftime("%d de %B del %Y", strtotime($json_cabecera[0]["FEC_REGISTRO"])):$fecha_elaboracion_informe;
            $prefijo_hora = ($json_cabecera[0]["FEC_ENVTRA"])?date('a',strtotime($json_cabecera[0]["FEC_ENVTRA"])):"";
            $fecha_ingreso_sistema_carta = ($json_cabecera[0]["FEC_ENVTRA"])?strftime("%d de %B del %Y", strtotime($json_cabecera[0]["FEC_ENVTRA"])):$fecha_ingreso_sistema_carta;
            $fecha_ingreso_sistema_hora = ($json_cabecera[0]["FEC_ENVTRA"])?strftime("%I:%M ".$prefijo_hora." del %d de %B del %Y", strtotime(date('Y-m-d H:i:s',strtotime($json_cabecera[0]["FEC_ENVTRA"])))):$fecha_ingreso_sistema_hora;
            
            $entidad_responsable =$json_cabecera[0]["NOM_ER"];;
        }
        
        curl_close ($curl_cabecera);

        /* Seteando servicio de actividades */

        $curl_detalle_actividades = curl_init();
        curl_setopt($curl_detalle_actividades, CURLOPT_URL,$this->web_url_servicio."/nue_lst_rp03");
        curl_setopt($curl_detalle_actividades, CURLOPT_POST, TRUE);
        curl_setopt($curl_detalle_actividades, CURLOPT_POSTFIELDS, "nucleo=".$nucleo."");
        curl_setopt($curl_detalle_actividades, CURLOPT_RETURNTRANSFER, true);
        $remote_server_detalle_actividades = curl_exec ($curl_detalle_actividades);

        if($remote_server_detalle_actividades){
            $json_detalle_actividades = Json::decode($json = $remote_server_detalle_actividades);
            //var_dump($json_detalle_actividades);die;
        }

        $template->setValue('NUCLEO_EJECUTOR', $nucleo_ejecutor);
        $template->setValue('CONVENIO', $convenio);
        $template->setValue('FECHA_INGRESO_SISTEMA', $fecha_ingreso_sistema);
        $template->setValue('FECHA_ELABORACION_INFORME', $fecha_elaboracion_informe);
        //$template->setValue('MEMORANDO', $memorando);
        $template->setValue('FECHA_INGRESO_SISTEMA_CARTA', $fecha_ingreso_sistema_carta);
        $template->setValue('FECHA_INGRESO_SISTEMA_HORA', $fecha_ingreso_sistema_hora);
        $template->setValue('ENTIDAD_RESPONSABLE', $entidad_responsable);

        $total_meta = 0.00 ;
        $monto_total_a = 0.00 ;
        if(count($json_detalle_actividades)>0){
            $template->cloneRow('nro_a', count($json_detalle_actividades));
            $rows = 1 ;
            
            foreach($json_detalle_actividades as $actividad){
                $imp_presupuesto=$actividad['IMP_PRESUPUESTO'];

                $template->setValue('nro_a#'.$rows.'', "".ucwords($rows)."");
                $template->setValue('codamcrd#'.$rows.'', "".ucwords($actividad['COD_AMCRD'])."");
                $template->setValue('actividad_a#'.$rows.'', "".ucwords($actividad['TXT_ACTIVIDAD'])."");
                $template->setValue('dpto#'.$rows.'', "".ucwords($actividad['TXT_DPTO'])."");
                $template->setValue('prov#'.$rows.'', "".ucwords($actividad['TXT_PROV'])."");
                $template->setValue('dist#'.$rows.'', "".ucwords($actividad['TXT_DIST'])."");
                $template->setValue('meta#'.$rows.'', "".ucwords($actividad['IMP_META_KM'])."");
                $template->setValue('monto_a#'.$rows.'', "".ucwords(number_format($imp_presupuesto, 2, '.', ','))."");

                $total_meta = $total_meta + $actividad['IMP_META_KM'];
                $monto_total_a = $monto_total_a + $imp_presupuesto;
                $rows++;
            }
        }

        $template->setValue('total_meta', $total_meta);
        $template->setValue('monto_total_a', "S/ ".ucwords(number_format(round($monto_total_a,0), 2, '.', ',')));
        $template->setValue('monto_total_a_texto', $this->numero_letras(round($monto_total_a)));
        

        $monto_total_cd = 0 ;
        $monto_total_ci = 0 ;
        $nro_actividades = 0 ;
        if(count($json_detalle_actividades)>0){
            $template->cloneRow('nro_b', count($json_detalle_actividades));
            $rows = 1 ;
            
            foreach($json_detalle_actividades as $actividad){
                $imp_presupuesto=$actividad['IMP_PRESUPUESTO'];
                $costo_directo=$imp_presupuesto*0.9;
                $costo_indirecto=$imp_presupuesto*0.1;

                $template->setValue('nro_b#'.$rows.'', "".ucwords($rows)."");
                $template->setValue('codamcrd_b#'.$rows.'', "".ucwords($actividad['COD_AMCRD'])."");
                $template->setValue('actividad_b#'.$rows.'', "".ucwords($actividad['TXT_ACTIVIDAD'])."");
                $template->setValue('monto_b#'.$rows.'', "".ucwords(number_format($imp_presupuesto, 2, '.', ','))."");
                $template->setValue('costo_directo#'.$rows.'',"".ucwords(number_format($costo_directo, 2, '.', ','))."");
                $template->setValue('costo_indirecto#'.$rows.'', "".ucwords(number_format($costo_indirecto, 2, '.', ','))."");

                $monto_total_cd = $monto_total_cd + $costo_directo;
                $monto_total_ci = $monto_total_ci + $costo_indirecto;
                $rows++;
                $nro_actividades++;
            }
        }

        $template->setValue('monto_total_cd', "S/ ".ucwords(number_format(round($monto_total_cd,0), 2, '.', ',')));
        $template->setValue('monto_total_cd_texto', $this->numero_letras(round($monto_total_cd,0)));
        $template->setValue('monto_total_ci', "S/ ".ucwords(number_format(round($monto_total_ci,0), 2, '.', ',')));
        $template->setValue('monto_total_ci_texto', $this->numero_letras(round($monto_total_ci,0)));
        $template->setValue('nro_actividades', $nro_actividades);
        
        $monto_total_elaboracion = 0 ;
        if(count($json_detalle_actividades)>0){
            $template->cloneRow('nro_c', count($json_detalle_actividades));
            $rows = 1 ;
            
            foreach($json_detalle_actividades as $actividad){
                $imp_presupuesto=$actividad['IMP_PRESUPUESTO'];
                $monto_elaboracion = ($imp_presupuesto*0.0035);

                $template->setValue('nro_c#'.$rows.'', "".ucwords($rows)."");
                $template->setValue('codamcrd_c#'.$rows.'', "".ucwords($actividad['COD_AMCRD'])."");
                $template->setValue('actividad_c#'.$rows.'', "".ucwords($actividad['TXT_ACTIVIDAD'])."");
                $template->setValue('costo_actividad#'.$rows.'', "".ucwords(number_format($imp_presupuesto, 2, '.', ','))."");
                $template->setValue('monto_elaboracion#'.$rows.'',  "".ucwords(number_format($monto_elaboracion, 2, '.', ','))."");

                $monto_total_elaboracion = $monto_total_elaboracion + $monto_elaboracion;
                $rows++;
            }
        }

        $monto_global = round($monto_total_elaboracion,0) + round($monto_total_a,0);
        $template->setValue('monto_elaboracion_real', "S/ ".ucwords(number_format($monto_total_elaboracion, 2, '.', ',')));
        $template->setValue('monto_total_elaboracion', "S/ ".ucwords(number_format(round($monto_total_elaboracion,0), 2, '.', ',')));
        $template->setValue('monto_total_elaboracion_texto', $this->numero_letras(round($monto_total_elaboracion,0)));
        $template->setValue('monto_global', "S/ ".ucwords(number_format($monto_global, 2, '.', ',')));
        $template->setValue('monto_global_texto', $this->numero_letras($monto_global));



        #reemplaza una variable con una imagen
        /*
        $aImgs = array(
            array(
                    'img' => 'phpword/Examples/_earth.JPG',
            'size' => array(200, 150),
            'dataImg' => 'Esta es el pie de imagen para _earth.JPG'
            )
        );
        $document->replaceStrToImg( 'unaImagen', $aImgs);*/


        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $template->saveAs($temp_file);
        header("Content-Disposition: attachment; filename=INFORME_TECNICO_DGIAR.docx");
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file);  // remove temp file
        return true;
    }

    public function actionOcopiMac($nucleo=null)
    {
        
        date_default_timezone_set('America/Lima');
        setlocale (LC_TIME,"spanish");

        $phpWord = new PhpWord();
        $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/formatos/INFORME_OCOPI_MAC.docx');
        
        $json_cabecera = [];
        $json_detalle_actividades = [];

        $nucleo_ejecutor = "";
        $memorando = "";
        $nro_informe = "";
        $siglas_evaluador = "";
        $fecha_ingreso_sistema = strftime("%d de %B del %Y", strtotime(date('d-m-Y')));
        $entidad_responsable = "";
        $nro_cut = "";
        $txt_apenom_pre = "";
        $cod_dnipre = "";
        $txt_apenom_sec = "";
        $cod_dnisec = "";
        $txt_apenom_tes = "";
        $cod_dnites = "";


        $convenio = "";
        
        
        

        /* Seteando servicio de cabecera del nucleo ejecutor */

        $curl_cabecera = curl_init();
        curl_setopt($curl_cabecera, CURLOPT_URL,$this->web_url_servicio."/nue_lst_rp02");
        curl_setopt($curl_cabecera, CURLOPT_POST, TRUE);
        curl_setopt($curl_cabecera, CURLOPT_POSTFIELDS, "nucleo=".$nucleo."");
        curl_setopt($curl_cabecera, CURLOPT_RETURNTRANSFER, true);
        $remote_server_cabecera = curl_exec ($curl_cabecera);
        $json_cabecera_validar = Json::decode($json = $remote_server_cabecera);
        if(count($json_cabecera_validar)==0){
            return json_encode(['msg'=>'No encontrado','flg'=>0]);
            //var_dump($json_cabecera_validar);die;
        }

        if($remote_server_cabecera){
            $json_cabecera = Json::decode($json = $remote_server_cabecera);
            $nucleo_ejecutor = $json_cabecera[0]["TXT_NOMBRE_NUCLEO"];
            $fecha_ingreso_sistema = ($json_cabecera[0]["FEC_ENVTRA"])?strftime("%d de %B del %Y", strtotime($json_cabecera[0]["FEC_ENVTRA"])):$fecha_ingreso_sistema;
            $entidad_responsable =$json_cabecera[0]["NOM_ER"];
            $nro_cut =$json_cabecera[0]["CUT"];

            $txt_apenom_pre = $json_cabecera[0]["TXT_NOMPRE"]." ".$json_cabecera[0]["TXT_APEPRE"];
            $cod_dnipre = $json_cabecera[0]["COD_DNIPRE"];
            $txt_apenom_sec = $json_cabecera[0]["TXT_NOMSEC"]." ".$json_cabecera[0]["TXT_APESEC"];
            $cod_dnisec = $json_cabecera[0]["COD_DNISEC"];
            $txt_apenom_tes = $json_cabecera[0]["TXT_NOMTES"]." ".$json_cabecera[0]["TXT_APETES"];
            $cod_dnites = $json_cabecera[0]["COD_DNITES"];

            //$convenio = $json_cabecera[0]["TXT_NROCONVENIO"];
        }
        curl_close ($curl_cabecera);

        $template->setValue('NUCLEO_EJECUTOR', $nucleo_ejecutor);
        $template->setValue('FECHA_INGRESO_SISTEMA', $fecha_ingreso_sistema);
        $template->setValue('ENTIDAD_RESPONSABLE', $entidad_responsable);
        $template->setValue('NRO_CUT', $nro_cut);
        $template->setValue('TXT_APENOM_PRE', $txt_apenom_pre);
        $template->setValue('COD_DNIPRE', $cod_dnipre);
        $template->setValue('TXT_APENOM_SEC', $txt_apenom_sec);
        $template->setValue('COD_DNISEC', $cod_dnisec);
        $template->setValue('TXT_APENOM_TES', $txt_apenom_tes);
        $template->setValue('COD_DNITES', $cod_dnites);
        /* Seteando servicio de actividades */

        $curl_detalle_actividades = curl_init();
        curl_setopt($curl_detalle_actividades, CURLOPT_URL,$this->web_url_servicio."/nue_lst_rp03");
        curl_setopt($curl_detalle_actividades, CURLOPT_POST, TRUE);
        curl_setopt($curl_detalle_actividades, CURLOPT_POSTFIELDS, "nucleo=".$nucleo."");
        curl_setopt($curl_detalle_actividades, CURLOPT_RETURNTRANSFER, true);
        $remote_server_detalle_actividades = curl_exec ($curl_detalle_actividades);

        if($remote_server_detalle_actividades){
            $json_detalle_actividades = Json::decode($json = $remote_server_detalle_actividades);
        }

        
        $total_meta = 0.00 ;
        $monto_total_a = 0.00 ;
        $nro_actividades = 0 ;
        if(count($json_detalle_actividades)>0){
            $template->cloneRow('NRO_A', count($json_detalle_actividades));
            $rows = 1 ;
            
            foreach($json_detalle_actividades as $actividad){
                $imp_presupuesto=round($actividad['IMP_PRESUPUESTO'],0);

                $template->setValue('NRO_A#'.$rows.'', "".ucwords($rows)."");
                $template->setValue('CODAMCRD#'.$rows.'', "".ucwords($actividad['COD_AMCRD'])."");
                $template->setValue('ACTIVIDAD_A#'.$rows.'', "".ucwords($actividad['TXT_ACTIVIDAD'])."");
                $template->setValue('DPTO#'.$rows.'', "".ucwords($actividad['TXT_DPTO'])."");
                $template->setValue('PROV#'.$rows.'', "".ucwords($actividad['TXT_PROV'])."");
                $template->setValue('DIST#'.$rows.'', "".ucwords($actividad['TXT_DIST'])."");
                $template->setValue('META#'.$rows.'', "".ucwords($actividad['IMP_META_KM'])."");
                $template->setValue('MONTO_A#'.$rows.'', "".ucwords(number_format($imp_presupuesto, 2, '.', ','))."");

                $total_meta = $total_meta + $actividad['IMP_META_KM'];
                $monto_total_a = $monto_total_a + $imp_presupuesto;
                $rows++;
                $nro_actividades++;
            }
        }

        $template->setValue('TOTAL_META', $total_meta);
        $template->setValue('MONTO_TOTAL_A', "S/ ".ucwords(number_format($monto_total_a, 2, '.', ',')));
        $template->setValue('MONTO_TOTAL_A_TEXTO', $this->numero_letras($monto_total_a));
        $template->setValue('NRO_ACTIVIDADES', $nro_actividades);

        /*
        if(count($json_detalle_actividades)>0){
            $template->cloneRow('nro_b', count($json_detalle_actividades));
            $rows = 1 ;
            $monto_total_cd = 0 ;
            $monto_total_ci = 0 ;
            $nro_actividades = 0 ;
            foreach($json_detalle_actividades as $actividad){
                $template->setValue('nro_b#'.$rows.'', "".ucwords($rows)."");
                $template->setValue('actividad_b#'.$rows.'', "".ucwords($actividad['TXT_ACTIVIDAD'])."");
                $template->setValue('monto_b#'.$rows.'', "".ucwords(number_format(round($actividad['IMP_PRESUPUESTO'],0), 2, '.', ','))."");
                $template->setValue('costo_directo#'.$rows.'',"".ucwords(number_format((round($actividad['IMP_PRESUPUESTO']*0.9,0)), 2, '.', ','))."");
                $template->setValue('costo_indirecto#'.$rows.'', "".ucwords(number_format((round($actividad['IMP_PRESUPUESTO']*0.1,0)), 2, '.', ','))."");

                $monto_total_cd = $monto_total_cd + (round($actividad['IMP_PRESUPUESTO']*0.9,0));
                $monto_total_ci = $monto_total_ci + (round($actividad['IMP_PRESUPUESTO']*0.1,0));
                $rows++;
                $nro_actividades++;
            }
        }

        $template->setValue('monto_total_cd', "S/ ".ucwords(number_format(round($monto_total_cd,0), 2, '.', ',')));
        $template->setValue('monto_total_cd_texto', $this->numero_letras(round($monto_total_cd,0)));
        $template->setValue('monto_total_ci', "S/ ".ucwords(number_format(round($monto_total_ci,0), 2, '.', ',')));
        $template->setValue('monto_total_ci_texto', $this->numero_letras(round($monto_total_ci,0)));
        $template->setValue('nro_actividades', $nro_actividades);*/
        
        $monto_total_elaboracion = 0 ;
        if(count($json_detalle_actividades)>0){
            $template->cloneRow('NRO_C', count($json_detalle_actividades));
            $rows = 1 ;
            
            foreach($json_detalle_actividades as $actividad){
                $imp_presupuesto=round($actividad['IMP_PRESUPUESTO'],0);
                $monto_elaboracion = round(($imp_presupuesto*0.0035),0);

                $template->setValue('NRO_C#'.$rows.'', "".ucwords($rows)."");
                $template->setValue('ACTIVIDAD_C#'.$rows.'', "".ucwords($actividad['TXT_ACTIVIDAD'])."");
                $template->setValue('COSTO_ACTIVIDAD#'.$rows.'', "".ucwords(number_format($imp_presupuesto, 2, '.', ','))."");
                $template->setValue('MONTO_ELABORACION#'.$rows.'',  "".ucwords(number_format($monto_elaboracion, 2, '.', ','))."");

                $monto_total_elaboracion = $monto_total_elaboracion + $monto_elaboracion;
                $rows++;
            }
        }

        $monto_global = $monto_total_elaboracion + $monto_total_a;

        $template->setValue('MONTO_TOTAL_ELABORACION', "S/ ".ucwords(number_format($monto_total_elaboracion, 2, '.', ',')));
        $template->setValue('MONTO_TOTAL_ELABORACION_TEXTO', $this->numero_letras($monto_total_elaboracion));
        $template->setValue('MONTO_GLOBAL', "S/ ".ucwords(number_format($monto_global, 2, '.', ',')));
        $template->setValue('MONTO_GLOBAL_TEXTO', $this->numero_letras($monto_global));




        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $template->saveAs($temp_file);
        header("Content-Disposition: attachment; filename=INFORME_OCOPI_MAC.docx");
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file);  // remove temp file
        return true;
    }

    
    public function actionInformeNe($id_nucleo=null)
    {
        
        date_default_timezone_set('America/Lima');
        setlocale (LC_TIME,"spanish");

        $phpWord = new PhpWord();
        $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/formatos/INFORME_NE.docx');
        
        $txt_er = "";
        $txt_ne = "";
        $txt_nombre_pre = "";
        /* Seteando servicio de cabecera del nucleo ejecutor */
        
        $curl_cabecera = curl_init();
        curl_setopt($curl_cabecera, CURLOPT_URL,$this->web_url_servicio."/nue_lst_rp02");
        curl_setopt($curl_cabecera, CURLOPT_POST, TRUE);
        curl_setopt($curl_cabecera, CURLOPT_POSTFIELDS, "nucleo=".$id_nucleo."");
        curl_setopt($curl_cabecera, CURLOPT_RETURNTRANSFER, true);
        $remote_server_cabecera = curl_exec ($curl_cabecera);
        $json_cabecera_validar = Json::decode($json = $remote_server_cabecera);
        /*if(count($json_cabecera_validar)==0){
            return json_encode(['msg'=>'No encontrado','flg'=>0]);
        }*/
        
        if(count($json_cabecera_validar)!=0){
            $json_cabecera = Json::decode($json = $remote_server_cabecera);
            //$nucleo_ejecutor = $json_cabecera[0]["TXT_NOMBRE_NUCLEO"];
            $txt_er = $json_cabecera[0]["NOM_ER"];
            $txt_ne = $json_cabecera[0]["TXT_NOMBRE_NUCLEO"];
            $txt_nombre_pre = $json_cabecera[0]["TXT_NOMPRE"].' '.$json_cabecera[0]["TXT_APEPRE"];
        }
        curl_close ($curl_cabecera);

        $template->setValue('TXT_ER', $txt_er);
        $template->setValue('TXT_NE', $txt_ne);
        $template->setValue('TXT_NOMBRE_PRE', $txt_nombre_pre);

        /* Seteando servicio de actividades */

        $curl_detalle_actividades = curl_init();
        curl_setopt($curl_detalle_actividades, CURLOPT_URL,$this->web_url_servicio."/nue_lst_ficha_nu_er_2");
        curl_setopt($curl_detalle_actividades, CURLOPT_POST, TRUE);
        curl_setopt($curl_detalle_actividades, CURLOPT_POSTFIELDS, "id_nucleo=".$id_nucleo."");
        curl_setopt($curl_detalle_actividades, CURLOPT_RETURNTRANSFER, true);
        $remote_server_detalle_actividades = curl_exec ($curl_detalle_actividades);

        if($remote_server_detalle_actividades){
            $json_detalle_actividades = Json::decode($json = $remote_server_detalle_actividades);
        }

        if(count($json_detalle_actividades)>0){
            $template->cloneRow('cod_amcrd', count($json_detalle_actividades));
            $rows = 1 ;
            foreach($json_detalle_actividades as $actividad){
                $template->setValue('cod_amcrd#'.$rows.'', "".ucwords($actividad['COD_AMCRD'])."");
                $template->setValue('rpta_a#'.$rows.'', "".ucwords($actividad['TXT_COORD_UTM'])."");
                $template->setValue('rpta_b#'.$rows.'', "".ucwords($actividad['TXT_LONG_MANTENIMIENTO'])."");
                $template->setValue('rpta_c#'.$rows.'', "".ucwords($actividad['TXT_MONTO_PREVISTO_SEGUN_FICHA'])."");
                $template->setValue('rpta_d#'.$rows.'', "".ucwords($actividad['TXT_PPTO_POR_PARTIDA'])."");
                $template->setValue('rpta_e#'.$rows.'', "".ucwords($actividad['TXT_CU_SUBPARTIDA'])."");
                $template->setValue('rpta_f#'.$rows.'', "".ucwords($actividad['TXT_CI_DISGREGADO'])."");
                $template->setValue('rpta_g#'.$rows.'', "".ucwords($actividad['TXT_UBI_INFR_INI_FIN'])."");
                $template->setValue('rpta_h#'.$rows.'', "".ucwords($actividad['TXT_VEGETACION_TENSA'])."");
                $template->setValue('rpta_i#'.$rows.'', "".ucwords($actividad['TXT_VEGETACION_LIGERA'])."");
                $template->setValue('rpta_j#'.$rows.'', "".ucwords($actividad['TXT_COLMATA_CANAL'])."");
                $template->setValue('rpta_k#'.$rows.'', "".ucwords($actividad['TXT_DESMON_CORONA'])."");
                $template->setValue('rpta_l#'.$rows.'', "".ucwords($actividad['TXT_NECESIDAD_MANT'])."");

                $rows++;
            }
        }

        if(count($json_detalle_actividades)>0){
            $template->cloneRow('cod_amcrd_a', count($json_detalle_actividades));
            $rows = 1 ;
            foreach($json_detalle_actividades as $actividad){
                $template->setValue('cod_amcrd_a#'.$rows.'', "".ucwords($actividad['COD_AMCRD'])."");
                $template->setValue('actividad#'.$rows.'', "".ucwords($actividad['TXT_ACTIVIDAD'])."");
                $rows++;
            }
        }

        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $template->saveAs($temp_file);
        header("Content-Disposition: attachment; filename=INFORME_NE.docx");
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file);  // remove temp file
        return true;
    }


    public function actionInformeDgiar($id_nucleo=null)
    {
        
        date_default_timezone_set('America/Lima');
        setlocale (LC_TIME,"spanish");

        $phpWord = new PhpWord();
        $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/formatos/INFORME_DGIAR_FICHA.docx');
        $txt_er = "";
        $txt_ne = "";
        $txt_nombre_pre = "";
        /* Seteando servicio de cabecera del nucleo ejecutor */
        
        $curl_cabecera = curl_init();
        curl_setopt($curl_cabecera, CURLOPT_URL,$this->web_url_servicio."/nue_lst_rp02");
        curl_setopt($curl_cabecera, CURLOPT_POST, TRUE);
        curl_setopt($curl_cabecera, CURLOPT_POSTFIELDS, "nucleo=".$id_nucleo."");
        curl_setopt($curl_cabecera, CURLOPT_RETURNTRANSFER, true);
        $remote_server_cabecera = curl_exec ($curl_cabecera);
        $json_cabecera_validar = Json::decode($json = $remote_server_cabecera);
        /*if(count($json_cabecera_validar)==0){
            return json_encode(['msg'=>'No encontrado','flg'=>0]);
        }*/
        
        if(count($json_cabecera_validar)!=0){
            $json_cabecera = Json::decode($json = $remote_server_cabecera);
            //$nucleo_ejecutor = $json_cabecera[0]["TXT_NOMBRE_NUCLEO"];
            $txt_er = $json_cabecera[0]["NOM_ER"];
            $txt_ne = $json_cabecera[0]["TXT_NOMBRE_NUCLEO"];
            $txt_nombre_pre = $json_cabecera[0]["TXT_NOMPRE"].' '.$json_cabecera[0]["TXT_APEPRE"];
        }
        curl_close ($curl_cabecera);

        //$template->setValue('TXT_ER', $txt_er);
        $template->setValue('TXT_NE', $txt_ne);
        //$template->setValue('TXT_NOMBRE_PRE', $txt_nombre_pre);


        /* Seteando servicio de actividades */

        $curl_detalle_actividades = curl_init();
        curl_setopt($curl_detalle_actividades, CURLOPT_URL,$this->web_url_servicio."/nue_lst_ficha_nu_er_2");
        curl_setopt($curl_detalle_actividades, CURLOPT_POST, TRUE);
        curl_setopt($curl_detalle_actividades, CURLOPT_POSTFIELDS, "id_nucleo=".$id_nucleo."");
        curl_setopt($curl_detalle_actividades, CURLOPT_RETURNTRANSFER, true);
        $remote_server_detalle_actividades = curl_exec ($curl_detalle_actividades);

        if($remote_server_detalle_actividades){
            $json_detalle_actividades = Json::decode($json = $remote_server_detalle_actividades);
        }

        if(count($json_detalle_actividades)>0){
            $template->cloneRow('cod_amcrd', count($json_detalle_actividades));
            $rows = 1 ;
            foreach($json_detalle_actividades as $actividad){
                $template->setValue('cod_amcrd#'.$rows.'', "".ucwords($actividad['COD_AMCRD'])."");
                $template->setValue('rpta_a#'.$rows.'', "".ucwords($actividad['TXT_COORD_UTM'])."");
                $template->setValue('rpta_b#'.$rows.'', "".ucwords($actividad['TXT_LONG_MANTENIMIENTO'])."");
                $template->setValue('rpta_c#'.$rows.'', "".ucwords($actividad['TXT_MONTO_PREVISTO_SEGUN_FICHA'])."");
                $template->setValue('rpta_d#'.$rows.'', "".ucwords($actividad['TXT_PPTO_POR_PARTIDA'])."");
                $template->setValue('rpta_e#'.$rows.'', "".ucwords($actividad['TXT_CU_SUBPARTIDA'])."");
                $template->setValue('rpta_f#'.$rows.'', "".ucwords($actividad['TXT_CI_DISGREGADO'])."");
                $template->setValue('rpta_g#'.$rows.'', "".ucwords($actividad['TXT_UBI_INFR_INI_FIN'])."");
                $template->setValue('rpta_h#'.$rows.'', "".ucwords($actividad['TXT_VEGETACION_TENSA'])."");
                $template->setValue('rpta_i#'.$rows.'', "".ucwords($actividad['TXT_VEGETACION_LIGERA'])."");
                $template->setValue('rpta_j#'.$rows.'', "".ucwords($actividad['TXT_COLMATA_CANAL'])."");
                $template->setValue('rpta_k#'.$rows.'', "".ucwords($actividad['TXT_DESMON_CORONA'])."");
                $template->setValue('rpta_l#'.$rows.'', "".ucwords($actividad['TXT_NECESIDAD_MANT'])."");

                $rows++;
            }
        }

        /* Seteando servicio de actividades 2 */

        $curl_detalle_actividades_2 = curl_init();
        curl_setopt($curl_detalle_actividades_2, CURLOPT_URL,$this->web_url_servicio."/nue_lst_rp03");
        curl_setopt($curl_detalle_actividades_2, CURLOPT_POST, TRUE);
        curl_setopt($curl_detalle_actividades_2, CURLOPT_POSTFIELDS, "nucleo=".$id_nucleo."");
        curl_setopt($curl_detalle_actividades_2, CURLOPT_RETURNTRANSFER, true);
        $remote_server_detalle_actividades_2 = curl_exec ($curl_detalle_actividades_2);

        if($remote_server_detalle_actividades_2){
            $json_detalle_actividades_2 = Json::decode($json = $remote_server_detalle_actividades_2);
        }

        $total_ppto_ficha = 0 ;
        $total_ppto_asignado = 0 ;
        if(count($json_detalle_actividades_2)>0){
            $template->cloneRow('F_COD_AMCRD', count($json_detalle_actividades_2));
            $rows = 1 ;
            
            foreach($json_detalle_actividades_2 as $actividad){
                $ppto_asignado=$actividad['IMP_PRESUPUESTO'];
                $meta = $actividad['IMP_META_KM'];
                $ppto_ficha = ($ppto_asignado*0.0035);

                $template->setValue('F_COD_AMCRD#'.$rows.'', "".ucwords($actividad['COD_AMCRD'])."");
                $template->setValue('ACTIVIDAD#'.$rows.'', "".ucwords($actividad['TXT_ACTIVIDAD'])."");
                $template->setValue('PPTO_ASIGNADO#'.$rows.'', "".ucwords(number_format($ppto_asignado, 2, '.', ','))."");
                $template->setValue('PPTO_FICHA#'.$rows.'',  "".ucwords(number_format($ppto_ficha, 2, '.', ','))."");
                $template->setValue('META#'.$rows.'',  "".ucwords(number_format($meta, 2, '.', ','))."");

                

                $total_ppto_asignado = $total_ppto_asignado + $ppto_asignado;
                $total_ppto_ficha = $total_ppto_ficha + $ppto_ficha;
                $rows++;
            }
        }

        $template->setValue('TOTAL_PPTO_ASIGNADO', $total_ppto_asignado);
        $template->setValue('TOTAL_PPTO_FICHA', $total_ppto_ficha);
        $template->setValue('TOTAL_PPTO', ($total_ppto_ficha + $total_ppto_asignado));



        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $template->saveAs($temp_file);
        header("Content-Disposition: attachment; filename=INFORME_DGIAR_FICHA.docx");
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file);  // remove temp file
        return true;
    }

    // END FUNCTION
     
    public function subfijo($xx)
    { // esta función regresa un subfijo para la cifra
		$xx = trim($xx);
		$xstrlen = strlen($xx);
		if ($xstrlen == 1 || $xstrlen == 2 || $xstrlen == 3)
		    $xsub = "";
		//
		if ($xstrlen == 4 || $xstrlen == 5 || $xstrlen == 6)
		    $xsub = "MIL";
		//
		return $xsub;
    }


    public function numero_letras($xcifra)
    {
		$xarray = [0 => "Cero",
		    1 => "UN", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE",
		    "DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE",
		    "VEINTI", 30 => "TREINTA", 40 => "CUARENTA", 50 => "CINCUENTA", 60 => "SESENTA", 70 => "SETENTA", 80 => "OCHENTA", 90 => "NOVENTA",
		    100 => "CIENTO", 200 => "DOSCIENTOS", 300 => "TRESCIENTOS", 400 => "CUATROCIENTOS", 500 => "QUINIENTOS", 600 => "SEISCIENTOS", 700 => "SETECIENTOS", 800 => "OCHOCIENTOS", 900 => "NOVECIENTOS"
		];
		$xcifra = trim($xcifra);
		$xlength = strlen($xcifra);
		$xpos_punto = strpos($xcifra, ".");
		$xaux_int = $xcifra;
		$xdecimales = "00";
		if (!($xpos_punto === false)) {
		    if ($xpos_punto == 0) {
			$xcifra = "0" . $xcifra;
			$xpos_punto = strpos($xcifra, ".");
		    }
		    $xaux_int = substr($xcifra, 0, $xpos_punto); // obtengo el entero de la cifra a covertir
		    $xdecimales = substr($xcifra . "00", $xpos_punto + 1, 2); // obtengo los valores decimales
		}
	     
		$XAUX = str_pad($xaux_int, 18, " ", STR_PAD_LEFT); // ajusto la longitud de la cifra, para que sea divisible por centenas de miles (grupos de 6)
		$xcadena = "";
		for ($xz = 0; $xz < 3; $xz++) {
		    $xaux = substr($XAUX, $xz * 6, 6);
		    $xi = 0;
		    $xlimite = 6; // inicializo el contador de centenas xi y establezco el límite a 6 dígitos en la parte entera
		    $xexit = true; // bandera para controlar el ciclo del While
		    while ($xexit) {
			if ($xi == $xlimite) { // si ya llegó al límite máximo de enteros
			    break; // termina el ciclo
			}
	     
			$x3digitos = ($xlimite - $xi) * -1; // comienzo con los tres primeros digitos de la cifra, comenzando por la izquierda
			$xaux = substr($xaux, $x3digitos, abs($x3digitos)); // obtengo la centena (los tres dígitos)
			for ($xy = 1; $xy < 4; $xy++) { // ciclo para revisar centenas, decenas y unidades, en ese orden
			    switch ($xy) {
				case 1: // checa las centenas
				    if (substr($xaux, 0, 3) < 100) { // si el grupo de tres dígitos es menor a una centena ( < 99) no hace nada y pasa a revisar las decenas
					 
				    } else {
					$key = (int) substr($xaux, 0, 3);
					if (TRUE === array_key_exists($key, $xarray)){  // busco si la centena es número redondo (100, 200, 300, 400, etc..)
					    $xseek = $xarray[$key];
					    $xsub = $this->subfijo($xaux); // devuelve el subfijo correspondiente (Millón, Millones, Mil o nada)
					    if (substr($xaux, 0, 3) == 100)
						$xcadena = " " . $xcadena . " CIEN " . $xsub;
					    else
						$xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
					    $xy = 3; // la centena fue redonda, entonces termino el ciclo del for y ya no reviso decenas ni unidades
					}
					else { // entra aquí si la centena no fue numero redondo (101, 253, 120, 980, etc.)
					    $key = (int) substr($xaux, 0, 1) * 100;
					    $xseek = $xarray[$key]; // toma el primer caracter de la centena y lo multiplica por cien y lo busca en el arreglo (para que busque 100,200,300, etc)
					    $xcadena = " " . $xcadena . " " . $xseek;
					} // ENDIF ($xseek)
				    } // ENDIF (substr($xaux, 0, 3) < 100)
				    break;
				case 2: // checa las decenas (con la misma lógica que las centenas)
				    if (substr($xaux, 1, 2) < 10) {
					 
				    } else {
					$key = (int) substr($xaux, 1, 2);
					if (TRUE === array_key_exists($key, $xarray)) {
					    $xseek = $xarray[$key];
					    $xsub = $this->subfijo($xaux);
					    if (substr($xaux, 1, 2) == 20)
						$xcadena = " " . $xcadena . " VEINTE " . $xsub;
					    else
						$xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
					    $xy = 3;
					}
					else {
					    $key = (int) substr($xaux, 1, 1) * 10;
					    $xseek = $xarray[$key];
					    if (20 == substr($xaux, 1, 1) * 10)
						$xcadena = " " . $xcadena . " " . $xseek;
					    else
						$xcadena = " " . $xcadena . " " . $xseek . " Y ";
					} // ENDIF ($xseek)
				    } // ENDIF (substr($xaux, 1, 2) < 10)
				    break;
				case 3: // checa las unidades
				    if (substr($xaux, 2, 1) < 1) { // si la unidad es cero, ya no hace nada
					 
				    } else {
					$key = (int) substr($xaux, 2, 1);
					$xseek = $xarray[$key]; // obtengo directamente el valor de la unidad (del uno al nueve)
					$xsub = $this->subfijo($xaux);
					$xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
				    } // ENDIF (substr($xaux, 2, 1) < 1)
				    break;
			    } // END SWITCH
			} // END FOR
			$xi = $xi + 3;
		    } // ENDDO
	     
		    if (substr(trim($xcadena), -5, 5) == "ILLON") // si la cadena obtenida termina en MILLON o BILLON, entonces le agrega al final la conjuncion DE
			$xcadena.= " DE";
	     
		    if (substr(trim($xcadena), -7, 7) == "ILLONES") // si la cadena obtenida en MILLONES o BILLONES, entoncea le agrega al final la conjuncion DE
			$xcadena.= " DE";
	     
		    // ----------- esta línea la puedes cambiar de acuerdo a tus necesidades o a tu país -------
		    if (trim($xaux) != "") {
			switch ($xz) {
			    case 0:
				if (trim(substr($XAUX, $xz * 6, 6)) == "1")
				    $xcadena.= "UN BILLON ";
				else
				    $xcadena.= " BILLONES ";
				break;
			    case 1:
				if (trim(substr($XAUX, $xz * 6, 6)) == "1")
				    $xcadena.= "UN MILLON ";
				else
				    $xcadena.= " MILLONES ";
				break;
			    case 2:
				if ($xcifra < 1) {
				    $xcadena = "CERO $xdecimales/100 SOLES";
				}
				if ($xcifra >= 1 && $xcifra < 2) {
				    $xcadena = "UN $xdecimales/100 SOLES";
				}
				if ($xcifra >= 2) {
				    $xcadena.= " $xdecimales/100 SOLES"; //
				}
				break;
			} // endswitch ($xz)
		    } // ENDIF (trim($xaux) != "")
		    // ------------------      en este caso, para México se usa esta leyenda     ----------------
		    $xcadena = str_replace("VEINTI ", "VEINTI", $xcadena); // quito el espacio para el VEINTI, para que quede: VEINTICUATRO, VEINTIUN, VEINTIDOS, etc
		    $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
		    $xcadena = str_replace("UN UN", "UN", $xcadena); // quito la duplicidad
		    $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
		    $xcadena = str_replace("BILLON DE MILLONES", "BILLON DE", $xcadena); // corrigo la leyenda
		    $xcadena = str_replace("BILLONES DE MILLONES", "BILLONES DE", $xcadena); // corrigo la leyenda
		    $xcadena = str_replace("DE UN", "UN", $xcadena); // corrigo la leyenda
		} // ENDFOR ($xz)
		return trim($xcadena);
    }

    public function DescripcionMes($mes=null)
    {
        setlocale (LC_TIME,"spanish");
        //$mes = 01;
        $mes=$mes-1;
        $fechaInicio = '2016-05-01';
        $nuevafecha = strtotime ( '+'.$mes.' month' , strtotime ( $fechaInicio ) ) ;
        $nuevafecha = strtoupper(strftime ( '%b %Y' , $nuevafecha ) ) ;
        return $nuevafecha;
    }

}
