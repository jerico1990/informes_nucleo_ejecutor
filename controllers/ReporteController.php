<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Settings;
use yii\helpers\Json;


class ReporteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public $web_url_servicio = "http://fspreset.minagri.gob.pe:5000";
    //public $web_url_servicio = "http://si2.minagri.gob.pe:4000";

    public function actionReporteErFicha($id_nucleo=null)
    {
        date_default_timezone_set('America/Lima');
        setlocale (LC_TIME,"spanish");
        $phpWord = new PhpWord();
        $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/formatos/REPORTE_ER_FICHA.docx');
        
        $json_cabecera = [];
        $json_detalle_actividades = [];

        $txt_cut = "";
        $txt_nombre_nucleo = "";
        $int_nro_fichas = "";

        /* Seteando servicio de cabecera del nucleo ejecutor */

        $curl_cabecera = curl_init();
        curl_setopt($curl_cabecera, CURLOPT_URL, $this->web_url_servicio . "/nue_lst_rp02");
        curl_setopt($curl_cabecera, CURLOPT_POST, TRUE);
        curl_setopt($curl_cabecera, CURLOPT_POSTFIELDS, "nucleo=".$id_nucleo."");
        curl_setopt($curl_cabecera, CURLOPT_RETURNTRANSFER, true);
        $remote_server_cabecera = curl_exec ($curl_cabecera);
        $json_cabecera_validar = Json::decode($json = $remote_server_cabecera);
        if(count($json_cabecera_validar)==0){
            //return json_encode(['msg'=>'No encontrado','flg'=>0]);
        }
        
        if(count($json_cabecera_validar)!=0 && $remote_server_cabecera){
            $json_cabecera = Json::decode($json = $remote_server_cabecera);
            $txt_nombre_nucleo = $json_cabecera[0]["TXT_NOMBRE_NUCLEO"];
            $txt_cut = $json_cabecera[0]["CUT"];
        }
        curl_close ($curl_cabecera);
        $template->setValue('TXT_NOMBRE_NUCLEO', $txt_nombre_nucleo);
        $template->setValue('TXT_CUT', $txt_cut);


        /* Seteando servicio de actividades */

        $curl_detalle_actividades = curl_init();
        curl_setopt($curl_detalle_actividades, CURLOPT_URL,$this->web_url_servicio."/nue_lst_ficha_nu_er_2");
        curl_setopt($curl_detalle_actividades, CURLOPT_POST, TRUE);
        curl_setopt($curl_detalle_actividades, CURLOPT_POSTFIELDS, "id_nucleo=".$id_nucleo."");
        curl_setopt($curl_detalle_actividades, CURLOPT_RETURNTRANSFER, true);
        $remote_server_detalle_actividades = curl_exec ($curl_detalle_actividades);

        if($remote_server_detalle_actividades){
            $json_detalle_actividades = Json::decode($json = $remote_server_detalle_actividades);
        }

        $t_meta=0;
        $t_costo_directo=0;
        $t_costo_indirecto=0;
        $t_costo_actividad=0;
        $t_ppto_asignado=0;
        $nro_fichas = 0;
        if(count($json_detalle_actividades)>0){
            $template->cloneRow('CODIGO_ACT', count($json_detalle_actividades));
            $rows = 1 ;
            foreach($json_detalle_actividades as $actividad){
                $template->setValue('CODIGO_ACT#'.$rows.'', "".ucwords($actividad['COD_AMCRD'])."");
                $template->setValue('ACTIVIDAD#'.$rows.'', "".ucwords($actividad['TXT_ACTIVIDAD'])."");
                $template->setValue('META#'.$rows.'', "".ucwords($actividad['IMP_META_KM'])."");
                $template->setValue('COSTO_DIRECTO#'.$rows.'', "".ucwords($actividad['DBL_COSTO_DIRECTO'])."");
                $template->setValue('COSTO_INDIRECTO#'.$rows.'', "".ucwords($actividad['DBL_COSTO_INDIRECTO'])."");
                $template->setValue('COSTO_ACTIVIDAD#'.$rows.'', "".ucwords($actividad['DBL_COSTO_TOTAL_FICHA'])."");
                $template->setValue('PPTO_ASIGNADO#'.$rows.'', "".ucwords($actividad['IMP_PRESUPUESTO'])."");

                $t_meta             = $t_meta + $actividad['IMP_META_KM'];
                $t_costo_directo    = $t_costo_directo + $actividad['DBL_COSTO_DIRECTO'];
                $t_costo_indirecto  = $t_costo_indirecto + $actividad['DBL_COSTO_INDIRECTO'];
                $t_costo_actividad  = $t_costo_actividad + $actividad['DBL_COSTO_TOTAL_FICHA'];
                $t_ppto_asignado    = $t_ppto_asignado + $actividad['IMP_PRESUPUESTO'];

                $nro_fichas = $rows;
                $rows++;
            }
        }

        $template->setValue('T_META', $t_meta);
        $template->setValue('T_COSTO_DIRECTO', $t_costo_directo);
        $template->setValue('T_COSTO_INDIRECTO', $t_costo_indirecto);
        $template->setValue('T_COSTO_ACTIVIDAD', $t_costo_actividad);
        $template->setValue('T_PPTO_ASIGNADO', $t_ppto_asignado);
        $template->setValue('INT_NRO_FICHAS', $nro_fichas);



        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $template->saveAs($temp_file);
        header("Content-Disposition: attachment; filename=REPORTE_ER_FICHA.docx");
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file);  // remove temp file
        return true;
    }
}
