<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Settings;
use yii\helpers\Json;


class SolicitudController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public $web_url_servicio = "http://fspreset.minagri.gob.pe:5000";
	//public $web_url_servicio = "http://si2.minagri.gob.pe:4000";
    
    public function actionSolicitudAprobacion()
    {
        date_default_timezone_set('America/Lima');
        setlocale (LC_TIME,"spanish");
        $phpWord = new PhpWord();
        $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/formatos/SOLICITUD_CONFORMIDAD_FICHA_TECNICA.docx');
        
        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $template->saveAs($temp_file);
        header("Content-Disposition: attachment; filename=ACTA_VERIFICACION.docx");
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file);  // remove temp file
        return true;
    }
}
